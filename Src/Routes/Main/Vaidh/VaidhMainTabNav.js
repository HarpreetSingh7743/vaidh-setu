import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { Icon } from 'native-base';
import Profile from '../../../Screens/Main/Vaidh/Profile';
import Dashboard from '../../../Screens/Main/Vaidh/Dashboard';
import ManageStore from '../../../Screens/Main/Vaidh/ManageStore';
import ManageAppointments from '../../../Screens/Main/Vaidh/ManageAppointments';

const VaidhMainTabNav = createBottomTabNavigator({
    Dashboard: {
        screen: Dashboard,
        navigationOptions: {
            title: "Home",
            tabBarIcon: () => {
                return (<Icon name="home" type="MaterialCommunityIcons" style={{color:'#fff'}}/>)
            }
        }
    },
    ManageStore: {
        screen: ManageStore,
        navigationOptions: {
            title: "My Store",
            tabBarIcon: () => {
                return (<Icon name="shop" type="Entypo" style={{color:'#fff'}}/>)
            }
        }
    },
    ManageAppointments: {
        screen: ManageAppointments,
        navigationOptions: {
            title: "Appointments",
            tabBarIcon: () => {
                return (<Icon name="calendar-multiple-check"  type="MaterialCommunityIcons" style={{color:'#fff'}}/>)
            }
        }
    },

    Profile: {
        screen: Profile,
        navigationOptions: {
            title: "Profile",
            tabBarIcon: () => {
                return (<Icon name="user" type="Entypo"  style={{color:'#fff'}}/>)
            }
        }
    },
}, {
    initialRouteName:'Dashboard',
    tabBarOptions:{
        activeBackgroundColor:'#01AE52',
        inactiveBackgroundColor:'#01AE52',
        inactiveTintColor:'#fff',
        activeTintColor:'#111'
        
    }
    

})
export default createAppContainer(VaidhMainTabNav)