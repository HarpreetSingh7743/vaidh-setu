import React from 'react';
import { createAppContainer } from "react-navigation";
import { createMaterialTopTabNavigator } from "react-navigation-tabs";
import TodaysAppointments from '../../../Screens/Main/Vaidh/TodaysAppointments';
import TommorowsAppointments from '../../../Screens/Main/Vaidh/TommorowsAppointments';


const MatNav= createMaterialTopTabNavigator({
TodaysAppointments:TodaysAppointments,
TommorowsAppointments:TommorowsAppointments
},{
    initialRouteName:'TodaysAppointments',
    tabBarOptions:{
        style:{
            backgroundColor:'#f11'
        },
        activeTintColor:'#fff',
        inactiveTintColor:'#111',
    },
    tabBarPosition:'none',
    swipeEnabled:true,
    lazy:true

});
export default createAppContainer(MatNav)