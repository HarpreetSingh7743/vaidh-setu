import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { Icon } from 'native-base';
import Dashboard from '../../../Screens/Main/YogaTeacher/Dashboard';
import ManageVideos from '../../../Screens/Main/YogaTeacher/ManageVideos';
import Profile from '../../../Screens/Main/YogaTeacher/Profile';


const YogaTeacherMainTabNav = createBottomTabNavigator({
    Dashboard: {
        screen: Dashboard,
        navigationOptions: {
            title: "Home",
            tabBarIcon: () => {
                return (<Icon name="home" type="MaterialCommunityIcons" style={{color:'#fff'}}/>)
            }
        }
    },
    ManageVideos: {
        screen: ManageVideos,
        navigationOptions: {
            title: "Videos",
            tabBarIcon: () => {
                return (<Icon name="folder-video" type="Entypo" style={{color:'#fff'}}/>)
            }
        }
    },

    Profile: {
        screen: Profile,
        navigationOptions: {
            title: "Profile",
            tabBarIcon: () => {
                return (<Icon name="user" type="Entypo"  style={{color:'#fff'}}/>)
            }
        }
    },
}, {
    initialRouteName:'Dashboard',
    tabBarOptions:{
        activeBackgroundColor:'#01AE52',
        inactiveBackgroundColor:'#01AE52',
        inactiveTintColor:'#fff',
        activeTintColor:'#111',
        labelStyle:{fontWeight:"bold"}
        
    }
    

})
export default createAppContainer(YogaTeacherMainTabNav)