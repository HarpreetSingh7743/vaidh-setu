import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Nearby from '../../../Screens/Main/User/Nearby'
import Dashboard from '../../../Screens/Main/User/Dashboard'
import Profile from '../../../Screens/Main/User/Profile'

import { Icon } from 'native-base';
import AllBlogs from '../../../Screens/Main/User/AllBlogs';


const UserMainTabNav = createBottomTabNavigator({
    Dashboard: {
        screen: Dashboard,
        navigationOptions: {
            title: "Home",
            tabBarIcon: () => {
                return (<Icon name="home" type="MaterialCommunityIcons" style={{color:'#fff'}}/>)
            }
        }
    },
    Nearby: {
        screen: Nearby,
        navigationOptions: {
            title: "Nearby",
            tabBarIcon: () => {
                return (<Icon name="search-location" type="FontAwesome5" style={{color:'#fff'}}/>)
            }
        }
    },
    AllBlogs: {
        screen: AllBlogs,
        navigationOptions: {
            title: "AllBlogs",
            tabBarIcon: () => {
                return (<Icon name="blogger"  type="MaterialCommunityIcons" style={{color:'#fff'}}/>)
            }
        }
    },

    Profile: {
        screen: Profile,
        navigationOptions: {
            title: "Profile",
            tabBarIcon: () => {
                return (<Icon name="user" type="Entypo"  style={{color:'#fff'}}/>)
            }
        }
    },
}, {
    initialRouteName:'Dashboard',
    tabBarOptions:{
        activeBackgroundColor:'#01AE52',
        inactiveBackgroundColor:'#01AE52',
        inactiveTintColor:'#fff',
        activeTintColor:'#111'
        
    }
    

})
export default createAppContainer(UserMainTabNav)