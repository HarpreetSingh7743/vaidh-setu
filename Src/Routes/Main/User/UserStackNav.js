import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AllProducts from '../../../Screens/Main/User/AllProducts';
import AllVaidhs from '../../../Screens/Main/User/AllVaidhs';
import AllYogaTeachers from '../../../Screens/Main/User/AllYogaTeachers';
import Appointments from '../../../Screens/Main/User/Appointments';
import Cart from '../../../Screens/Main/User/Cart';
import UserWishlist from '../../../Screens/Main/User/UserWishlist';
import ViewTeacherProfile from '../../../Screens/Main/User/ViewTeacherProfile';
import ViewTeacherVideos from '../../../Screens/Main/User/ViewTeacherVideos';
import ViewVaidhProduct from '../../../Screens/Main/User/ViewVaidhProduct';
import ViewVaidhProfile from '../../../Screens/Main/User/ViewVaidhProfile';
import ViewVaidhStore from '../../../Screens/Main/User/ViewVaidhStore';
import UserMainTabNav from './UserMainTabNav';
import UserFollowingPeoples from '../../../Screens/Main/User/UserFollowingPeoples';
import AddAppointment from '../../../Screens/Main/User/AddAppointment';


const MainAppNav = createStackNavigator({
    UserMainTabNav:UserMainTabNav,
    AllProducts:AllProducts,
    AllVaidhs:AllVaidhs,
    AllYogaTeachers:AllYogaTeachers,
    Appointments:Appointments,
    Cart:Cart,
    AddAppointment:AddAppointment,
    UserWishlist:UserWishlist,
    ViewTeacherProfile:ViewTeacherProfile,
    ViewTeacherVideos:ViewTeacherVideos,
    ViewVaidhProduct:ViewVaidhProduct,
    ViewVaidhProfile:ViewVaidhProfile,
    ViewVaidhStore:ViewVaidhStore,
    UserFollowingPeoples:UserFollowingPeoples
  },{
      initialRouteName:'UserMainTabNav',
      headerMode:'null'
  });
  
  export default createAppContainer(MainAppNav);