import React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AccountType from '../../Screens/Auth/AccountType';
import SignIn from '../../Screens/Auth/SignIn';
import UserDetails from '../../Screens/Auth/UserDetails'

const AuthAppNav = createStackNavigator({
    SignIn:SignIn,
    AccountType:AccountType,
    UserDetails:UserDetails,
  },{
      initialRouteName:'SignIn',
      headerMode:'null'
  });
  
  export default createAppContainer(AuthAppNav);