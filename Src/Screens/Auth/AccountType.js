import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { Radio,Toast } from 'native-base';

class AccountType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            AccountType:"",
            radio1: false,
            radio2: false,
            radio3: false
        };
    }

    handleClick(){
        if(this.state.AccountType!=""){
            this.props.navigation.navigate("UserDetails",{ AccountType:this.state.AccountType})
        }
        else{
            Toast.show({
                text: "Select Account Type",
                textStyle: { textAlign: "center", color: '#fff' },
                position:"bottom",
                duration: 2000,
                style: { borderRadius: 10, margin: responsiveWidth(10), backgroundColor: '#01AE52' }
            });
        }
    }
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                <View>
                    <Text style={{ color: '#01AE52', fontSize: responsiveFontSize(5), fontWeight: 'bold', marginTop: responsiveWidth(3) }}>Account Type!     </Text>
                    <Text style={{ color: '#888' }}>Please select type of your Account</Text>
                    <View style={{ marginTop: responsiveWidth(4) }}>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginBottom: responsiveWidth(3) }} onPress={() => { this.setState({ radio1: true, radio2: false, radio3: false,AccountType:"YogaTeacher" }) }}>
                            <Radio selected={this.state.radio1} color="#01AE52" selectedColor="#01AE52" onPress={() => { this.setState({ radio1: true, radio2: false, radio3: false ,AccountType:"YogaTeacher"}) }}/>
                            <View style={{ marginLeft: responsiveWidth(2) }}>
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(2.5), fontWeight: 'bold' }} >Yoga Teacher</Text>
                                <Text>I am a Yoga Teacher</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginBottom: responsiveWidth(3) }} onPress={() => { this.setState({ radio1: false, radio2: true, radio3: false,AccountType:"Vaidh" }) }}>
                            <Radio selected={this.state.radio2} color="#01AE52" selectedColor="#01AE52"  onPress={() => { this.setState({ radio1: false, radio2: true, radio3: false,AccountType:"Vaidh" }) }} />
                            <View style={{ marginLeft: responsiveWidth(2) }}>
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(2.5), fontWeight: 'bold' }}>Vaidh</Text>
                                <Text>I am a Vaidh</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginBottom: responsiveWidth(3) }} onPress={() => { this.setState({ radio1: false, radio2: false, radio3: true ,AccountType:"User"}) }}>
                            <Radio selected={this.state.radio3} color="#01AE52" selectedColor="#01AE52" onPress={() => { this.setState({ radio1: false, radio2: false, radio3: true ,AccountType:"User"}) }}/>
                            <View style={{ marginLeft: responsiveWidth(2) }}>
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(2.5), fontWeight: 'bold' }}>User</Text>
                                <Text>I am a normal User</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                <TouchableOpacity style={{
                    width: responsiveWidth(80),
                    height: responsiveHeight(6),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderColor: "#01AE52",
                    borderWidth: 2,
                    borderRadius: 15,
                    marginTop:responsiveWidth(6)
                }} onPress={()=>{this.handleClick()}}>
                    <Text style={{ color: '#01AE52', fontSize: responsiveFontSize(1.6), fontWeight: 'bold', }} >Confirm</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default AccountType;
