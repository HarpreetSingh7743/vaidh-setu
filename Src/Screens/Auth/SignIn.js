import React, { Component } from 'react';
import { View, Text, Image, TextInput, TouchableOpacity } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import Styles from '../../Components/Stylesheet/Styles';
import EnterOTPModal from '../../Components/Modals/EnterOTPModal';

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserMobile: "",
      VisibleModal: false,
      OTP: ""
    };
  }

  render() {
    var val=0
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor:"#fff"}}>

        {this.state.VisibleModal && <EnterOTPModal
         UserMobile={this.state.UserMobile}
         OTP={this.state.OTP}  
         onSubmit={(val)=>{this.setState({VisibleModal:val}),this.props.navigation.navigate("AccountType")}} 
         onExit={(val) => { this.setState({ VisibleModal: val }) }} />}

        <View style={{ height: responsiveHeight(60), width: responsiveWidth(90) }}>
          <Image source={require("../../Components/Images/Icons/Logo.png")} style={Styles.logoStyle} />
          <Text style={{ color: '#01AE52', fontSize: responsiveFontSize(5), fontWeight: 'bold', marginTop: responsiveWidth(3) }}>Hello There!</Text>
          <Text style={{ color: '#666', fontSize: responsiveFontSize(3.5), marginTop: responsiveWidth(3) }}>Welcome</Text>
          <Text style={{ color: '#888' }}>SignIn to continue with your mobile number</Text>

          <View style={{ marginTop: responsiveWidth(9) }}>
            <TextInput style={Styles.authInput}
              placeholder="Enter Mobile Number"
              onChangeText={(UserMobile) => { this.setState({ UserMobile: UserMobile }) }}
              keyboardType="number-pad" />
          </View>
          <Text style={{ color: '#888' }}>A 4 digit OTP will be sent via SMS to verify your Mobile Number</Text>

          <View style={{ flex: 0.3, alignItems: 'center', marginTop: responsiveHeight(6) }}>
            <TouchableOpacity style={Styles.FullLimeGreenButton} onPress={() => {
              this.setState({ VisibleModal: true }),
              val = Math.floor(1000 + Math.random() * 9000),
              this.setState({OTP:val})
              console.log(val);}}>
              <Text style={Styles.whiteBoldText}>SignIn</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default SignIn;
