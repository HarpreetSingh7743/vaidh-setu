import React, { Component } from "react";
import { Picker, Radio, Label, } from "native-base";
import { View, Image, Text, TextInput, TouchableOpacity } from "react-native";
import { responsiveFontSize, responsiveWidth, responsiveHeight } from "react-native-responsive-dimensions";
import Styles from "../../Components/Stylesheet/Styles";

export default class PickerExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      AccountType: this.props.navigation.state.params.AccountType,
      Cities: [
        { label: "Andhra Pradesh", value: "key2" },
        { label: "Arunachal Pradesh", value: "key3" },
        { label: "Assam", value: "key4" },
        { label: "Bihar", value: "key5" },
        { label: "Chandigarh", value: "key6" },
        { label: "Chhattisgarh", value: "key7" },
        { label: "Delhi", value: "key8" },
        { label: "Goa", value: "key9" },
        { label: "Gujarat", value: "key10" },
        { label: "Haryana", value: "key11" },
        { label: "Himachal Pradesh", value: "key12" },
        { label: "Jammu and Kashmir", value: "key13" },
        { label: "Jharkhand", value: "key14" },
        { label: "Karnataka", value: "key15" },
        { label: "Kerala", value: "key16" },
        { label: "Lakshadweep", value: "key17" },
        { label: "Madhya Pradesh", value: "key18" },
        { label: "Maharashtra", value: "key19" },
        { label: "Manipur", value: "key20" },
        { label: "Meghalaya", value: "key21" },
        { label: "Mizoram", value: "key22" },
        { label: "Nagaland", value: "key23" },
        { label: "Orissa", value: "key24" },
        { label: "Puducherry", value: "key25" },
        { label: "Punjab", value: "key26" },
        { label: "Rajasthan", value: "key27" },
        { label: "Sikkim", value: "key28" },
        { label: "Tamil Nadu", value: "key29" },
        { label: "Telangana", value: "key30" },
        { label: "Tripura", value: "key31" },
        { label: "Uttar Pradesh", value: "key32" },
        { label: "Uttarakhand", value: "key33" },
        { label: "West Bengal", value: "key34" },
      ],
      FirstName: "",
      LastName: "",
      Address: "",
      ZIPcode: "",
      selectedState: "key0",
      selectedCity: "key0",
      State: "",
      City: "",
      radio1: false,
      radio2: false,
      YogaCentreAddress: "",
      VaidhType: ""
    };
  }
  renderScreen() {
    if (this.state.AccountType == "User") {
      return (
        <View></View>
      )
    }
    else if (this.state.AccountType == "Vaidh") {
      return (
        <View style={{ width: responsiveWidth(90), marginTop: responsiveWidth(3) }}>
          <TextInput style={Styles.authInput}
            placeholder="Enter Specialization"
            keyboardType="default"
            onChangeText={(VaidhType) => { this.setState({ VaidhType: VaidhType }) }}
            returnKeyType="go" />
        </View>
      )
    }
    else if (this.state.AccountType == "YogaTeacher") {
      return (
        <View style={{ marginTop: responsiveWidth(3) }}>
          <View style={{ flexDirection: 'row', marginBottom: responsiveWidth(2) }}>
            <TouchableOpacity style={{ flex: 0.5, flexDirection: 'row', paddingLeft: responsiveWidth(2) }}
              onPress={() => { this.setState({ radio1: true, radio2: false }) }}>
              <Radio onPress={() => { this.setState({ radio1: true, radio2: false }) }} selected={this.state.radio1} color="#01AE52" selectedColor="#01AE52" style={{ marginRight: responsiveWidth(2) }} />
              <Text>I'm a Yoga Teacher</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 0.5, flexDirection: 'row', paddingLeft: responsiveWidth(2) }}
              onPress={() => { this.setState({ radio1: false, radio2: true }) }}>
              <Radio onPress={() => { this.setState({ radio1: false, radio2: true }) }} selected={this.state.radio2} color="#01AE52" selectedColor="#01AE52" style={{ marginRight: responsiveWidth(2) }} />
              <Text>I own a Yoga center</Text>
            </TouchableOpacity>

          </View>
          {this.state.radio2 && <TextInput style={Styles.authInput}
            placeholder="Yoga Center Address "
            keyboardType="default"
            onChangeText={(YogaCentreAddress) => { this.setState({ YogaCentreAddress: YogaCentreAddress }) }}
            returnKeyType="go" />}
        </View>
      )
    }
  }
  renderCities() {
    return this.state.Cities.map((t) => {
      return (
        <Picker.Item key={t.value} label={t.label} value={t.value} />
      )
    })
  }
  onValueChange = (value = String, label) => {
    this.setState({
      selectedState: value,
      State: label
    });
  }
  onValueChange1 = (value = String, label) => {
    this.setState({
      selectedCity: value,
      City: label
    });
  }
  render() {
    return (

      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
        <View style={{ height: responsiveHeight(60), width: responsiveWidth(90) }}>
          <Text style={{ color: '#01AE52', fontSize: responsiveFontSize(5), fontWeight: 'bold', }}>Enter Details</Text>
          <Text style={{ color: '#888' }}>Enter Your Personal details</Text>
          <View style={{ marginTop: responsiveWidth(4), flexDirection: 'row' }}>
            <View style={{ flex: 0.6, paddingRight: responsiveWidth(4) }}>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Enter First Name</Text>
              <TextInput style={Styles.authInput}
                placeholder="First Name"
                keyboardType="default"
                onChangeText={(FirstName) => { this.setState({ FirstName: FirstName }) }}
                returnKeyType="next"
                onSubmitEditing={() => { this.refs.LastName.focus() }} />
            </View>
            <View style={{ flex: 0.4, paddingLeft: responsiveWidth(4) }}>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Enter Last Name</Text>
              <TextInput style={Styles.authInput}
                placeholder="Last Name"
                keyboardType="default"
                onChangeText={(LastName) => { this.setState({ LastName: LastName }) }}
                returnKeyType="next"
                onSubmitEditing={() => { this.refs.Address.focus() }}
                ref={'LastName'} />
            </View>
          </View>
          <Text style={{ fontSize: responsiveFontSize(1.3) }}>Enter your Address</Text>
          <TextInput style={Styles.authInput}
            placeholder="Address"
            keyboardType="default"
            onChangeText={(Address) => { this.setState({ Address: Address }) }}
            returnKeyType="next"
            onSubmitEditing={() => { this.refs.ZIPcode.focus() }}
            ref={'Address'} />

          <Text style={{ fontSize: responsiveFontSize(1.3) }}>Enter ZIP Code</Text>
          <TextInput style={Styles.authInput}
            placeholder="ZIP Code"
            onChangeText={(ZIPcode) => { this.setState({ ZIPcode: ZIPcode }) }}
            keyboardType="number-pad"
            returnKeyType="done"
            ref={'ZIPcode'}
          />

          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.5, }}>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Select State</Text>
              <View style={{ width: responsiveWidth(40), height: responsiveHeight(5), borderWidth: 1, borderColor: '#01AE52', borderRadius: 10, alignItems: 'center', justifyContent: 'center', }}>
                <Picker
                  note
                  mode="dropdown"
                  placeholder="Select State"
                  style={{ width: responsiveWidth(40) }}
                  selectedValue={this.state.selectedState}
                  onValueChange={this.onValueChange.bind(this)}
                >
                  {this.renderCities()}
                </Picker>
              </View></View>
            <View style={{ flex: 0.5, }}>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Select City</Text>
              <View style={{ width: responsiveWidth(40), height: responsiveHeight(5), borderWidth: 1, borderColor: '#01AE52', borderRadius: 10, alignItems: 'center', justifyContent: 'center', }}>
                <Picker
                  note
                  mode="dropdown"
                  placeholder="Select State"
                  style={{ width: responsiveWidth(40) }}
                  selectedValue={this.state.selectedCity}
                  onValueChange={this.onValueChange1.bind(this)}
                >
                  {this.renderCities()}
                </Picker>
              </View>
            </View>
          </View>
          {this.renderScreen()}
          <View style={{ flex: 0.15, alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(8) }}>
            <TouchableOpacity style={{
              width: responsiveWidth(80),
              height: responsiveHeight(6),
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: "#01AE52",
              borderWidth: 3,
              borderRadius: 15,
              marginTop: responsiveWidth(6)
            }}
              onPress={() => {
                alert("FullName: " + this.state.FirstName + this.state.LastName + " Email: " + this.state.UserEmail + " Address: " + this.state.Address + " ZipCode: " + this.state.ZIPcode + " State " + this.state.Cities[this.state.State].label + " City: " + this.state.Cities[this.state.City].label)
              }}>
              <Text style={{ color: '#01AE52', fontSize: responsiveFontSize(1.6), fontWeight: 'bold', }} >Done</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}