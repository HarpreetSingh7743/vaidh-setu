import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import AppointmentsNav from '../../../Routes/Main/Vaidh/AppointmentsNav';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon } from 'native-base';

class ManageAppointments extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7,  }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Manage Appointments</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                            <TouchableOpacity>
                                <Icon name="search" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 0.91 }}>
                    <AppointmentsNav />
                </View>
            </View>
        );
    }
}

export default ManageAppointments;
