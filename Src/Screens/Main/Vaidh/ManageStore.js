import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { Avatar } from 'react-native-elements';
import { Card } from 'native-base';
import AddProductModal from '../../../Components/Modals/Vaidh/AddProductModal';
import EditProductModal from '../../../Components/Modals/Vaidh/EditProductModal';

class ManageStore extends Component {
    constructor(props) {
        super(props);
        this.state = {
            VisibleAddProductModal:false,
            VisibleEditProductModal:false,
            MostLikedItems: [
                {
                    key: 1,
                    ProductName: "Cureveda Joint Care Oil",
                    ProductRatings: 4.5,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2018/10/Joint-Care-Oil-Mock-Up-768x768.jpg",
                    ProductPrice: 545
                }, {
                    key: 2,
                    ProductName: "Cureveda Gluco Guide",
                    ProductRatings: 4.3,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Gluco-1-768x768.jpg",
                    ProductPrice: 520
                },
                {
                    key: 3,
                    ProductName: "Cureveda Sleep Sure",
                    ProductRatings: 4.1,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Sleep-Sure-1-768x768.jpg",
                    ProductPrice: 450
                },
                {
                    key: 4,
                    ProductName: "Cureveda Brain Boost",
                    ProductRatings: 3.9,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Brain-Boost-1-768x768.jpg",
                    ProductPrice: 560
                },
            ],

            StoreItems: [
                {
                    key: 1,
                    ProductName: "Cureveda Joint Care Oil",
                    ProductRatings: 4.5,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2018/10/Joint-Care-Oil-Mock-Up-768x768.jpg",
                    ProductPrice: 545
                },
                {
                    key: 2,
                    ProductName: "Cureveda Lax Light",
                    ProductRatings: 4.3,
                    ProductImge: "https://cureveda.com/wp-content/uploads/Lax-Light-Tab-New-Mock-Up-768x768.jpg",
                    ProductPrice: 395
                },
                {
                    key: 3,
                    ProductName: "Cureveda Pureprash",
                    ProductRatings: 4.2,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2019/01/Pureprash-Mock-UP-768x768.jpg",
                    ProductPrice: 600
                },
                {
                    key: 4,
                    ProductName: "Cureveda Gluco Guide",
                    ProductRatings: 4.3,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Gluco-1-768x768.jpg",
                    ProductPrice: 520
                },
                {
                    key: 5,
                    ProductName: "Cureveda Sleep Sure",
                    ProductRatings: 4.1,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Sleep-Sure-1-768x768.jpg",
                    ProductPrice: 450
                },
                {
                    key: 6,
                    ProductName: "Cureveda Brain Boost",
                    ProductRatings: 3.9,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Brain-Boost-1-768x768.jpg",
                    ProductPrice: 560
                },
            ],
        };
    }
    MostLikedItems() {
        return this.state.MostLikedItems.map((t) => {
            let ProductImge = t.ProductImge
            return (
                <View key={t.key} style={{ height: responsiveHeight(19), width: responsiveWidth(30), marginVertical: responsiveWidth(1.4), marginHorizontal: responsiveWidth(1.4) }}>
                    <Card style={{ height: responsiveHeight(19), width: responsiveWidth(30), padding: responsiveWidth(1),borderRadius:10 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Avatar
                                source={{ uri: ProductImge }} size="large" />
                        </View>
                        <Text style={{ fontWeight: 'bold' }}>
                            {t.ProductName}
                        </Text>
                        <Text>Price : ₹{t.ProductPrice}</Text>
                    </Card>
                </View>
            )
        })
    }
    renderStoreItems() {
        return this.state.StoreItems.map((t) => {
            let ProductImge = t.ProductImge
            return (
                <TouchableOpacity key={t.key} style={{ height: responsiveHeight(19), width: responsiveWidth(30), marginVertical: responsiveWidth(1.4), marginHorizontal: responsiveWidth(1.4) }}
                onPress={()=>{this.setState({VisibleEditProductModal:true})}}>

                   
                    <Card style={{ height: responsiveHeight(19), width: responsiveWidth(30), padding: responsiveWidth(1),borderRadius:10 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Avatar
                                source={{ uri: ProductImge }} size="large" />
                        </View>
                        <Text style={{ fontWeight: 'bold' }}>
                            {t.ProductName}
                        </Text>
                        <Text>Price :  ₹{t.ProductPrice}</Text>
                    </Card>
                </TouchableOpacity>
            )
        })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7, flexDirection: 'row' }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Store Management</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>

                        </View>
                    </View>
                </View>

                <View style={{ flex: 0.91, alignItems: 'center', justifyContent: 'center',backgroundColor:"#fff" }}>

                   {this.state.VisibleAddProductModal && <AddProductModal onExit={(val)=>{this.setState({VisibleAddProductModal:val})}}/>}
                   { this.state.VisibleEditProductModal && <EditProductModal onExit={(val)=>{this.setState({VisibleEditProductModal:val})}}/>}
                   
                    <ScrollView>
                        <Text style={{ fontSize: responsiveFontSize(2.3), fontWeight: 'bold', marginTop: responsiveWidth(2), marginLeft: responsiveWidth(2) }}>Most Liked Products</Text>
                        <View style={{
                            flexDirection: 'row',
                            width: responsiveWidth(99),
                            flexWrap: "wrap",
                        }}>
                            { this.MostLikedItems()}
                        </View>
                        <Text style={{ fontSize: responsiveFontSize(2.3), fontWeight: 'bold', marginTop: responsiveWidth(15), marginLeft: responsiveWidth(2) }}>Store Items</Text>
                        <View style={{
                            flexDirection: 'row',
                            width: responsiveWidth(99),
                            flexWrap: "wrap",
                        }}>
                            <TouchableOpacity style={{ height: responsiveHeight(17), width: responsiveWidth(30), marginVertical: responsiveWidth(1.4), marginHorizontal: responsiveWidth(1.4) }}
                            onPress={()=>{this.setState({VisibleAddProductModal:true})}}>
                                <Card style={{ height: responsiveHeight(17), width: responsiveWidth(30), padding: responsiveWidth(1),alignItems: 'center',justifyContent: 'center',borderRadius:10 }}>
                                    <Avatar source={require("../../../Components/Images/Icons/Buttons/AddProduct.png")} size="large" />
                                    <Text style={{ fontWeight: 'bold' }}>
                                        Add Product
                                    </Text>
                                </Card>
                            </TouchableOpacity>
                            {this.renderStoreItems()}
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default ManageStore;
