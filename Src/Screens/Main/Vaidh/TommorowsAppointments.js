import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../../../Components/Stylesheet/Styles';
import { Icon, Card, CardItem } from 'native-base';
import RescheduleAppointmentModal from '../../../Components/Modals/Vaidh/RescheduleAppointmentModal';

class TommorowsAppointments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleRescheduleAppointmentModal: false,
            Temp: [],
            AppointmentsDetails: [
                {
                    id: 1,
                    PatientName: "Harpreet Singh",
                    PatientAge: "22",
                    PatientContact: "9898989898",
                    AppointmentTime: "10:00 AM",
                    PatientSituation: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."
                },
                {
                    id: 2,
                    PatientName: "Ankush Kumar",
                    PatientAge: "19",
                    PatientContact: "6547895852",
                    AppointmentTime: "12:00 PM",
                    PatientSituation: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."
                },
                {
                    id: 3,
                    PatientName: "Santosh Kumar",
                    PatientAge: "25",
                    PatientContact: "9854782102",
                    AppointmentTime: "01:30 PM",
                    PatientSituation: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."
                },
            ]
        };
    }

    cancelAppointments(t) {

        let Patient = t.PatientName
        Alert.alert(
            "Cancel Appointment",
            "Are you sure you want to cancel " + Patient + "'s Appointment?",
            [
                {
                    text: "No",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "Yes", onPress: () => {
                        let Temp = this.state.AppointmentsDetails
                        let pos = Temp.indexOf(t)
                        Temp.splice(pos, 1)
                        this.setState({ AppointmentsDetails: Temp })
                    }
                }
            ],
            { cancelable: false }
        );
    }

    renderAppointments() {
        return this.state.AppointmentsDetails.map((t) => {
            return (
                <View key={t.id} style={{ width: responsiveWidth(96), }}>
                    <Card style={{ width: responsiveWidth(95), borderRadius: 10 }}>
                        <CardItem style={{ backgroundColor: '#01AE52', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold' }}>{t.PatientName} ( Age: {t.PatientAge} )</Text>
                        </CardItem>
                        <View style={{ paddingHorizontal: responsiveWidth(2) }}>
                            <Text style={{ fontSize: responsiveFontSize(1.6), fontWeight: 'bold' }}>Contact : <Text style={{ fontWeight: 'normal' }}>{t.PatientContact}</Text></Text>
                            <Text style={{ fontSize: responsiveFontSize(1.6), fontWeight: 'bold' }}>Appointment Details : <Text style={{ fontWeight: 'normal' }}>{t.AppointmentTime}</Text></Text>
                            <Text style={{ fontSize: responsiveFontSize(1.6), fontWeight: 'bold' }}>Patient Problem : <Text style={{ fontWeight: 'normal' }}>{t.PatientSituation}</Text></Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginVertical: responsiveWidth(2) }}>
                            <TouchableOpacity style={Styles.BorderLimeGreenButton} onPress={() => { this.cancelAppointments(t) }}>
                                <Text style={{ color: '#01AE52', fontWeight: 'bold', textAlign: 'center' }}>Cancel Appointment</Text>
                            </TouchableOpacity>

                            {this.state.VisibleRescheduleAppointmentModal && <RescheduleAppointmentModal PatientDetails={this.state.Temp} onExit={(val) => { this.setState({ VisibleRescheduleAppointmentModal: val }) }} />}

                            <TouchableOpacity style={Styles.FullLimeGreenButton} onPress={async () => {
                                await this.setState({ Temp: t })
                                this.setState({ VisibleRescheduleAppointmentModal: true })
                            }}>
                                <Text style={Styles.whiteBoldText}>Reschedule</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </View>
            )
        })
    }
    showDate() {
        var date = new Date().getDate() + 1;
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        var Tommorow = date + "/" + month + "/" + year
        return (
            <Text style={{ fontSize: responsiveFontSize(2.5), fontWeight: 'bold', color: "#666" }}>Tommorow : {Tommorow}</Text>
        )
    }
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {this.showDate()}
                    {this.renderAppointments()}
                </ScrollView>
            </View>
        );
    }
}

export default TommorowsAppointments;
