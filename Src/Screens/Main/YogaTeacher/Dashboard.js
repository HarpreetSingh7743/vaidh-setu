import React, { Component } from 'react';
import { View, Text, ImageBackground, TextInput, ScrollView ,Alert} from 'react-native';
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import { Icon, Card } from 'native-base';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import WritePostModal from '../../../Components/Modals/WritePostModal';
import { Avatar } from 'react-native-elements';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TeacherName: "Jaspreet Kaur",
      TeacherLocation: "#123, Sector 11, Chandigarh",
      TeacherProfile: "https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-3/img/user-04.3ac7ddd3.jpg ",
      TeacherPosts: [
        { key: 1, PostDate: "12/03/2020", PostTime: "21:00", Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books", Likes: 32, Dislikes: 12 },
        { key: 2, PostDate: "09/03/2020", PostTime: "10:00", Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books", Likes: 51, Dislikes: 11 },
        { key: 3, PostDate: "05/03/2020", PostTime: "17:00", Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books", Likes: 35, Dislikes: 14 },
        { key: 4, PostDate: "02/03/2020", PostTime: "22:00", Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books", Likes: 44, Dislikes: 8 },
      ],
      VisibleWritePostModal: false
    };
  }
  deletePost(t){
    let Temp=this.state.TeacherPosts
    let pos= Temp.indexOf(t)
    Alert.alert(
      "Remove Post",
      "Are you sure you want to delete this Post ?",
      [
          {
              text: "No",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
          },
          {
              text: "Yes", onPress: () => {
                  Temp.splice(pos, 1)
                  this.setState({ VaidhPosts: Temp })
              }
          }
      ],
      { cancelable: false }
  ); 
  }
  renderPosts() {
    return this.state.TeacherPosts.map((t) => {
      return (
        <View key={t.key} style={{ width: responsiveWidth(90), marginVertical: responsiveWidth(1), alignItems: 'center', }}>
          <Card style={{ width: responsiveWidth(99), padding: responsiveWidth(1.8), }}>
            <View style={{ flexDirection: 'row', marginBottom: responsiveWidth(2) }}>
              <View style={{ flex: 0.15 }}>
                <Avatar source={{ uri: this.state.TeacherProfile }} size="medium" rounded={true} />
              </View>
              <View style={{ flex: 0.65 }}>
                <Text style={{ fontWeight: 'bold' }}>{this.state.TeacherName}</Text>
                <Text style={{ fontSize: responsiveFontSize(1), fontWeight: 'bold' }}>{t.PostDate} at {t.PostTime}</Text>
              </View>
              <View style={{ flex: 0.2, alignItems: 'flex-end', }}>
                <TouchableOpacity onPress={() => { this.deletePost(t) }}>
                  <Text style={{ fontSize: responsiveFontSize(1.2), color: '#f33', textDecorationLine: "underline" }}>Remove</Text>
                </TouchableOpacity>
              </View>
            </View>
            <Text style={{ fontSize: responsiveFontSize(1.5) }}>{t.Post}</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}><Text style={{ fontSize: responsiveFontSize(1) }}>_______________________________________________________________________________</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <TouchableOpacity style={{ flexDirection: 'row', width: responsiveWidth(45), height: responsiveHeight(4), alignItems: 'center', justifyContent: 'center', }}>
                <Icon name="like2" type="AntDesign" style={{ color: '#666' }} />
                <Text style={{ color: '#666', fontSize: responsiveFontSize(1.5), fontWeight: 'bold', marginLeft: responsiveWidth(1) }}>Like ({t.Likes})</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ flexDirection: 'row', width: responsiveWidth(45), height: responsiveHeight(4), alignItems: 'center', justifyContent: 'center', }}>
                <Icon name="dislike2" type="AntDesign" style={{ color: '#666' }} />
                <Text style={{ color: '#666', fontSize: responsiveFontSize(1.5), fontWeight: 'bold', marginLeft: responsiveWidth(1) }}>Dislike ({t.Dislikes})</Text>
              </TouchableOpacity>
            </View>
          </Card>
        </View>
      )
    })
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
          <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
            <View style={{ flex: 0.7, flexDirection: 'row' }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Home</Text>
            </View>
            <View style={{ flex: 0.3, alignItems: 'flex-end' }}>

            </View>
          </View>

        </View>
        <View style={{ flex: 0.91, backgroundColor: '#fff' }}>
          <ScrollView>
            <Text style={{ fontSize: responsiveFontSize(3), fontWeight: 'bold', marginTop: responsiveWidth(2), marginLeft: responsiveWidth(2) }}>Hello, <Text style={{ color: "#01AE52" }}>{this.state.TeacherName}</Text></Text>
            <Text style={{ fontSize: responsiveFontSize(1.5), fontWeight: 'bold', marginLeft: responsiveWidth(2) }}>{this.state.TeacherLocation}</Text>
            <View style={{ width: responsiveWidth(99), height: responsiveHeight(13), paddingVertical: responsiveWidth(2), alignItems: 'center', }}>

              {this.state.VisibleWritePostModal && <WritePostModal onExit={(val) => { this.setState({ VisibleWritePostModal: val }) }} />}
              <Text style={{
                width: responsiveWidth(95),
                height: responsiveHeight(10),
                borderWidth: 0.5,
                borderRadius: 20,
                padding: responsiveWidth(2),
                marginVertical: responsiveWidth(1),
                color: '#666'
              }} onPress={() => { this.setState({ VisibleWritePostModal: true }) }}>
                Write Something here
                 </Text>

            </View>
            <View style={{ width: responsiveWidth(100), alignItems: 'center', }}>
              {this.renderPosts()}
            </View>
          </ScrollView>
        </View>
      </View >
    );
  }
}


export default Dashboard;
