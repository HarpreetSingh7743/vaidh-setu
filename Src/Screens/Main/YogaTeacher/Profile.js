import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, TouchableOpacity, ScrollView, Switch, TextInput } from 'react-native';
import { Icon, Card, CardItem } from 'native-base';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../../../Components/Stylesheet/Styles';
import { Avatar } from 'react-native-elements';
import EditProfileModal from '../../../Components/Modals/YogaTeacher/EditProfileModal';


class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      VisibleEditProfileModal:false,
      AvatarSource: "https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-3/img/user-04.3ac7ddd3.jpg",
      Description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
      YogaTeacherName: "Jaspreet Kaur",
      YogaTeacherAge: "22",
      YogaTeacherContact: "9876543210",
      YogaTeacherAddress: "House No. 1234,Sector 11, Chandigarh",
      State: "Chandigarh",
      City: "Chandigarh",
      PinCode: 198879,
      YogaTeacherFollowers: 1100,
      YogaTeacherRatings: 4.3,
      YogaTeacherPosts: 43,
    };
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
          <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
            <View style={{ flex: 0.7, }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Profile</Text>
            </View>
            <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
              <TouchableOpacity>

                <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{ color: '#fff' }} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={{ flex: 0.91 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.4, padding: responsiveWidth(1), }}>
              <Avatar source={{ uri: this.state.AvatarSource }} showAccessory size="xlarge" rounded={true} onAccessoryPress={() => { alert("Insert ImagePicker here") }} />
            </View>
            <View style={{ flex: 0.6, justifyContent: 'center', }}>
              <Text style={{ color: '#01AE52', fontSize: responsiveWidth(6), fontWeight: 'bold' }}>{this.state.YogaTeacherName}</Text>
              <Text style={{ fontSize: responsiveWidth(3), fontWeight: 'bold' }}>+91 {this.state.YogaTeacherContact}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(4) }}>
            <View style={{ flex: 0.32, alignItems: 'center', justifyContent: 'center', }}>
              <Text style={{ fontSize: responsiveFontSize(1.6), fontWeight: 'bold', color: '#666' }}>POSTS</Text>
              <Text style={{ textAlign: 'center' }}>{this.state.YogaTeacherPosts}</Text>
            </View>
            <View style={{ flex: 0.35, alignItems: 'center', justifyContent: 'center', }}>
              <Text style={{ fontSize: responsiveFontSize(1.6), fontWeight: 'bold', color: '#666' }}>FOLLOWERS</Text>
              <Text style={{ textAlign: 'center' }}>{this.state.YogaTeacherFollowers}</Text>
            </View>
            <View style={{ flex: 0.32, alignItems: 'center', justifyContent: 'center', }}>
              <Text style={{ fontSize: responsiveFontSize(1.6), fontWeight: 'bold', color: '#666' }}>RATINGS</Text>
              <Text style={{ textAlign: 'center', }}><Icon name="star" type="Entypo" style={{ fontSize: responsiveFontSize(2), color: '#FFD700' }} />{this.state.YogaTeacherRatings}</Text>
            </View>
          </View>
          <Card style={{ padding: responsiveWidth(2) }}>
            <View style={{ flexDirection: 'row' }}>


              {this.state.VisibleEditProfileModal && <EditProfileModal onExit={(val) => { this.setState({ VisibleEditProfileModal: val }) }} />}


              <Text style={{ color: '#01AE52', fontSize: responsiveWidth(5), fontWeight: 'bold' }}>About</Text>
              <TouchableOpacity onPress={() => { this.setState({ VisibleEditProfileModal: true }) }}>
                <Icon name="edit" type="Feather" style={{ fontSize: responsiveFontSize(2.3), color: '#01AE52', marginLeft: responsiveWidth(3) }} />
              </TouchableOpacity>
            </View>
            <View style={{ width: responsiveWidth(99), marginTop: responsiveWidth(2), flexDirection: 'row', flexWrap: 'wrap' }}>
              <Text style={{ fontWeight: 'bold', width: responsiveWidth(30) }}>Age </Text><Text style={{ width: responsiveWidth(65) }}> {this.state.YogaTeacherAge}</Text>
              <Text style={{ fontWeight: 'bold', width: responsiveWidth(30) }}>Address</Text><Text style={{ width: responsiveWidth(65) }}> {this.state.YogaTeacherAddress}</Text>
              <Text style={{ fontWeight: 'bold', width: responsiveWidth(30) }}>PinCode</Text><Text style={{ width: responsiveWidth(65) }}> {this.state.PinCode}</Text>
              <Text style={{ fontWeight: 'bold', width: responsiveWidth(30) }}>State</Text><Text style={{ width: responsiveWidth(65) }}> {this.state.State}</Text>
              <Text style={{ fontWeight: 'bold', width: responsiveWidth(30) }}>City</Text><Text style={{ width: responsiveWidth(65) }}> {this.state.City}</Text>
            </View>
          </Card>
          <Card style={{ padding: responsiveWidth(2) }}>
            <Text style={{ color: '#01AE52', fontSize: responsiveWidth(5), fontWeight: 'bold' }}>Description</Text>
            <View style={{ marginTop: responsiveWidth(2), }}>
              <Text style={{ fontWeight: 'normal', }}>{this.state.Description} </Text>
            </View>
          </Card>

        </View>
      </View>
    );
  }
}

export default Profile;
