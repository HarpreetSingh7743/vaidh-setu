import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image, Alert } from 'react-native';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon, Card } from 'native-base';
import { Avatar } from 'react-native-elements';
import AddVideoModal from '../../../Components/Modals/YogaTeacher/AddVideoModal';

class ManageVideos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      VisibleAddVideoModal: false,
      Profile: "https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-3/img/user-04.3ac7ddd3.jpg",
      videoInfo: [
        {
          key: 1,
          Title: "Top 20 Yoga Videos",
          Thumbnail: "https://www.42yogis.com/images/top-20-yoga-videos.jpg",
          Description: "just a Description to view text size and color ",
          UploadTime: "09:00 AM",
          UploadDate: "22/09/2019"
        },
        {
          key: 2,
          Title: "45 Minutes Yoga Class",
          Thumbnail: "https://i.ytimg.com/vi/Kjq1U8oWvHc/maxresdefault.jpg",
          Description: "just a Description to view text size and color",
          UploadTime: "06:00 AM",
          UploadDate: "21/09/2019"
        },
        {
          key: 3,
          Title: "Benifits of Online Yoga Class",
          Thumbnail: "https://www.ekamyogashala.com/blog/wp-content/uploads/2018/08/online-Yoga-Classes-ekamyogahala.jpg",
          Description: "just a Description to view text size and color",
          UploadTime: "08:00 AM",
          UploadDate: "15/09/2019"
        },
        {
          key: 4,
          Title: "Yoga in Home",
          Thumbnail: "https://www.wellandgood.com/wp-content/uploads/2017/08/Stocksy-Woman-Home-Yoga-Kate-Daigneault.jpg",
          Description: "just a Description to view text size and color",
          UploadTime: "07:00 AM",
          UploadDate: "14/09/2019"
        },

      ]
    };
  }

  deletePost(t) {
    let Temp = this.state.videoInfo
    let pos = Temp.indexOf(t)

    Alert.alert(
      "Remove Post",
      "Are you sure you want to delete this Post ?",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "Yes", onPress: () => {
            Temp.splice(pos, 1)
            this.setState({ VaidhPosts: Temp })
          }
        }
      ],
      { cancelable: false }
    );
  }
  renderVideos() {
    return this.state.videoInfo.map((t) => {
      return (
        <View key={t.key} style={{ width: responsiveWidth(95), marginVertical: responsiveWidth(1) }}>
          <Card style={{ width: responsiveWidth(95), padding: responsiveWidth(3), borderRadius: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 0.15 }}>
                <Avatar source={{ uri: this.state.Profile }} size="medium" rounded={true} />
              </View>
              <View style={{ flex: 0.75, }}>
                <Text style={{ color: '#333', fontSize: responsiveFontSize(1.8), fontWeight: 'bold', }}>{t.Title}</Text>
                <Text style={{ color: "#333", fontSize: responsiveFontSize(1.1), fontWeight: 'bold' }}>{t.UploadDate} at {t.UploadTime}</Text>
                <Text style={{ fontSize: responsiveFontSize(1.3), }}>{t.Description}</Text>
              </View>
              <View style={{ flex: 0.1, alignItems: 'flex-end', }}>
                <TouchableOpacity onPress={() => { this.deletePost(t) }}>
                  <Icon name="close" type="MaterialCommunityIcons" />
                </TouchableOpacity>
              </View>
            </View>

            <TouchableOpacity style={{ alignSelf: 'center', marginTop: responsiveWidth(2) }}>
              <Image source={{ uri: t.Thumbnail }} style={{ height: responsiveHeight(25), width: responsiveWidth(90) }} />
            </TouchableOpacity>
          </Card>
        </View>
      )
    })
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
          <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
            <View style={{ flex: 0.7, }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Manage Videos</Text>
            </View>
            <View style={{ flex: 0.3, alignItems: 'flex-end' }}>

              {this.state.VisibleAddVideoModal && <AddVideoModal onExit={(val) => { this.setState({ VisibleAddVideoModal: val }) }} />}
              <TouchableOpacity onPress={() => { this.setState({ VisibleAddVideoModal: true }) }}>
                <Icon name="add" type="MaterialIcons" style={{ color: '#fff' }} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={{ flex: 0.91, backgroundColor: '#f0f0f0', alignItems: 'center', justifyContent: 'center', }}>
          <ScrollView>
            {this.renderVideos()}
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default ManageVideos;
