import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { Icon, Card } from 'native-base';
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { Avatar, Rating } from 'react-native-elements';
import ChangeDeliveryAddressModal from '../../../Components/Modals/User/ChangeDeliveryAddressModal';

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleChangeDeliveryAddressModal: false,
            DeliveryAddress: "House No. 1234, Sector 11, Chandigarh",
            tatalBill: 2500,
            CartItems: [
                {
                    key: 1,
                    ProductName: "Cureveda Joint Care Oil",
                    SellerName: "Naveen Kumar",
                    ProductRatings: 4.5,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2018/10/Joint-Care-Oil-Mock-Up-768x768.jpg",
                    ProductPrice: 545,
                    ProductQuantity: 1
                },
                {
                    key: 2,
                    ProductName: "Cureveda Lax Light",
                    SellerName: "Ranjan Verma",
                    ProductRatings: 4.3,
                    ProductImge: "https://cureveda.com/wp-content/uploads/Lax-Light-Tab-New-Mock-Up-768x768.jpg",
                    ProductPrice: 395,
                    ProductQuantity: 1
                },
                {
                    key: 3,
                    ProductName: "Cureveda Pureprash",
                    SellerName: "Rakesh Kumar",
                    ProductRatings: 4.2,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2019/01/Pureprash-Mock-UP-768x768.jpg",
                    ProductPrice: 600,
                    ProductQuantity: 1
                },
                {
                    key: 6,
                    ProductName: "Cureveda Brain Boost",
                    SellerName: "Robin Singh",
                    ProductRatings: 3.9,
                    ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Brain-Boost-1-768x768.jpg",
                    ProductPrice: 560,
                    ProductQuantity: 1
                },

            ],
        };
    }

    removeItem() {
        //add code here
    }
    addItem() {
        //add code here
    }

    renderItems() {
        return this.state.CartItems.map((t) => {
            let ProductImge = t.ProductImge
            return (
                <Card key={t.key} style={{ paddingVertical: responsiveWidth(2), width: responsiveWidth(99), marginBottom: responsiveWidth(2) }}>
                    <View style={{ marginHorizontal: responsiveWidth(2), flexDirection: 'row' }}>
                        <View style={{ flex: 0.75 }}>
                            <Text style={{ color: '#333', fontWeight: 'bold', fontSize: responsiveFontSize(1.8) }}>{t.ProductName}</Text>
                            <Text style={{ fontSize: responsiveFontSize(1.3) }}>Seller : {t.SellerName}</Text>
                            <Text style={{ fontSize: responsiveFontSize(1.5), fontWeight: 'bold' }}>Price :  ₹{t.ProductPrice}</Text>
                            <Rating
                                type="star"
                                fractions={1}
                                imageSize={20}
                                startingValue={t.Ratings}
                                readonly
                                style={{ alignSelf: 'flex-start' }} />
                        </View>
                        <View style={{ flex: 0.25, alignItems: 'center', justifyContent: 'center', }}>
                            <Avatar source={{ uri: ProductImge }} size="large" />
                            <View style={{ flexDirection: 'row', marginTop: responsiveWidth(4) }}>
                                <TouchableOpacity style={{
                                    height: responsiveHeight(2.5),
                                    width: responsiveWidth(6),
                                    backgroundColor: '#01AE52',
                                    borderTopLeftRadius: 10,
                                    borderBottomLeftRadius: 10,
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}
                                    onPress={() => { this.removeItem() }}>
                                    <Icon name="md-remove" type="Ionicons" style={{ color: '#fff', fontSize: responsiveFontSize(1.3) }} />
                                </TouchableOpacity>
                                <Text style={{ fontWeight: 'bold', marginHorizontal: responsiveWidth(2) }}>{t.ProductQuantity}</Text>

                                <TouchableOpacity style={{
                                    height: responsiveHeight(2.5),
                                    width: responsiveWidth(6),
                                    backgroundColor: '#01AE52',
                                    borderTopRightRadius: 10,
                                    borderBottomRightRadius: 10,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                                    onPress={() => { this.addItem(t) }}>
                                    <Icon name="md-add" type="Ionicons" style={{ color: '#fff', fontSize: responsiveFontSize(1.3) }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: responsiveWidth(3), marginTop: responsiveWidth(4), alignItems: 'center' }}>
                        <TouchableOpacity style={{
                            flex: 0.5,
                            alignItems: 'center',
                            borderTopLeftRadius: 15,
                            borderBottomLeftRadius: 15,
                            borderWidth: 0.4,
                            borderColor: '#777',
                            paddingVertical: responsiveWidth(1.5),
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row'
                        }}>
                            <Icon name="heart" type="FontAwesome" style={{ fontSize: responsiveFontSize(1.5), color: '#f11' }} />
                            <Text style={{ fontSize: responsiveFontSize(1.5), color: '#666', marginLeft: responsiveWidth(1) }}>Add to Wishlist</Text>
                        </TouchableOpacity>
                        
                        <TouchableOpacity style={{
                            flex: 0.5,
                            alignItems: 'center',
                            borderTopRightRadius: 15,
                            borderBottomRightRadius: 15,
                            borderWidth: 0.4,
                            borderColor: '#777',
                            paddingVertical: responsiveWidth(1.5),
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row'
                        }}>
                            <Icon name="trash" type="FontAwesome5" style={{ fontSize: responsiveFontSize(1.5), color: '#666', }} />
                            <Text style={{ fontSize: responsiveFontSize(1.5), color: '#666', marginLeft: responsiveWidth(1) }}>Remove Item</Text>
                        </TouchableOpacity>

                    </View>
                </Card>
            )
        })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.state.visibleChangeDeliveryAddressModal && <ChangeDeliveryAddressModal onExit={(val) => { this.setState({ visibleChangeDeliveryAddressModal: val }) }} />}

                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(7.5), marginHorizontal: responsiveWidth(3), alignItems: 'center', justifyContent: 'center', }}>
                        <View style={{ flex: 0.7, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Cart</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                            <TouchableOpacity style={{ paddingHorizontal: responsiveWidth(3), flexDirection: 'row', paddingVertical: responsiveWidth(1.5), backgroundColor: '#fff', borderRadius: 15, alignItems: 'center' }}>
                                <Text style={{ color: '#01AE52', fontWeight: 'bold', fontSize: responsiveFontSize(1.6) }}>Checkout  </Text>
                                <Text style={{ fontSize: responsiveFontSize(1.3) }}>₹{this.state.tatalBill}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 0.85, backgroundColor: '#f0f0f0' }}>
                    <ScrollView>
                        {this.renderItems()}
                    </ScrollView>
                </View>
                <View style={{ flex: 0.06, backgroundColor: '#01AE52', paddingHorizontal: responsiveWidth(1), justifyContent: 'center', }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.75 }}>
                            <Text style={{ color: '#fff', marginTop: responsiveWidth(1) }}>Address:  {this.state.DeliveryAddress}</Text>
                        </View>
                        <View style={{ flex: 0.25, alignItems: 'flex-end', }}>
                            <TouchableOpacity style={{ paddingHorizontal: responsiveWidth(3), paddingVertical: responsiveWidth(2), backgroundColor: '#fff', borderRadius: 15 }}
                                onPress={() => { this.setState({ visibleChangeDeliveryAddressModal: true }) }}>
                                <Text style={{ color: '#01AE52', fontWeight: 'bold' }}>Change</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

export default Cart;
