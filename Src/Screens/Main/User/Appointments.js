import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../../../Components/Stylesheet/Styles';
import { Icon, Card, CardItem } from 'native-base';

class Appointments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            AppointmentsDetails: [
                {
                    id: 1,
                    VaidhName: "Dr. Sumit Khanna",
                    Location: "Homopathic Clinic,Sector 38-C, Chandigarh",
                    PatientName: "Harpreet Singh",
                    PatientAge: "22",
                    AppointmentTime: "10:00 AM",
                    AppointmentDate: "12/07/2020"
                },
                {
                    id: 2,
                    VaidhName: "Dr. Kanika Kapoor",
                    Location: "Ayur Clinic,Sector 41-D, Chandigarh",
                    PatientName: "Ankush Kumar",
                    PatientAge: "19",
                    AppointmentTime: "12:00 PM",
                    AppointmentDate: "12/07/2020"
                },
                {
                    id: 3,
                    VaidhName: "Dr. Akhil Aggarwal",
                    Location: "Patanjali Home, Sector 55, Chandigarh",
                    PatientName: "Santosh Kumar",
                    PatientAge: "25",
                    AppointmentTime: "01:30 PM",
                    AppointmentDate: "13/07/2020"
                },
            ]
        };
    }

    cancelAppointments(t) {

        let Vaidh=t.VaidhName
        Alert.alert(
            "Cancel Appointment",
            "Are you sure you want to cancel "+Vaidh+"'s Appointment?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "OK", onPress: () => {
                        let Temp = this.state.AppointmentsDetails
                        let pos = Temp.indexOf(t)
                        Temp.splice(pos, 1)
                        this.setState({ AppointmentsDetails: Temp })
                    }
                }
            ],
            { cancelable: false }
        );
    }

    renderAppointments() {
        return this.state.AppointmentsDetails.map((t) => {
            return (
                <View key={t.id} style={{ width: responsiveWidth(95), }}>
                    <Card style={{ width: responsiveWidth(95), borderRadius: 10 }}>
                        <CardItem style={{ backgroundColor: '#01AE52', borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold' }}>{t.VaidhName}</Text>
                        </CardItem>
                        <View style={{ paddingHorizontal: responsiveWidth(2) }}>
                            <Text style={{ fontWeight: 'bold' }}>Patient Name : <Text style={{ fontWeight: 'normal' }}>{t.PatientName}</Text></Text>
                            <Text style={{ fontWeight: 'bold' }}>Patient Age : <Text style={{ fontWeight: 'normal' }}>{t.PatientAge}</Text></Text>
                            <Text style={{ fontWeight: 'bold' }}>Appointment Location : <Text style={{ fontWeight: 'normal' }}>{t.Location}</Text></Text>
                            <Text style={{ fontWeight: 'bold' }}>Appointment Details : <Text style={{ fontWeight: 'normal' }}>{t.AppointmentDate} at {t.AppointmentTime}</Text></Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginVertical: responsiveWidth(2) }}>
                            <TouchableOpacity style={Styles.BorderLimeGreenButton} onPress={() => { this.cancelAppointments(t) }}>
                                <Text style={Styles.LimeGreenBoldText}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={Styles.FullLimeGreenButton}>
                                <Text style={Styles.whiteBoldText}>Get Directions</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </View>
            )
        })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Appointments</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                            <TouchableOpacity>
                                <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{ flex: 0.91, alignItems: 'center' }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {this.renderAppointments()}
                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default Appointments;
