import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon, Card } from 'native-base';
import { Avatar, Rating } from 'react-native-elements';
import Styles from '../../../Components/Stylesheet/Styles';
import WriteReview from '../../../Components/Modals/WriteReview';

class ViewVaidhProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleWriteReviewModal: false,
      ProductName: "Cureveda Sleep Sure",
      ProductSeller: "Krishna Verma",
      ProductDescription: "For deep & calm sleep support Sleep quality, deep sleep in sleepless nights & insomnia, Time to fall asleep, regularizing sleep cycle and keeping the mind relaxed, Duration of sleep, Promotes tranquility , calming effect & relaxation, Valerian root, Sarpagandha, Jatamansi, 30 vegetarian tablets, 15-30 day pack, 100% herbal, safe for long term use, no side effects",
      ProductRatings: 4.5,
      ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Sleep-Sure-1-768x768.jpg",
      ProductPrice: 545,
      ProductReviews: [
        { id: 1, Ratings: 2.7, UserName: "Shubham", RivewDate: "12/04/2020", Rivew: "Because of my work I need to travel from one place to another and I am not getting enough sleep. This tablet is very helpful for me to get enough sleep and feel relaxed." },
        { id: 2, Ratings: 4.1, UserName: "Seema", RivewDate: "09/04/2020", Rivew: "Overall These medicine Improved My Sleep. It also Helped in Reducing My Stress. These Tablets are Very High Quality and Works Effectively to Most of The People. Overall a Very Good Tablets" },
        { id: 3, Ratings: 4.3, UserName: "Yash", RivewDate: "01/04/2020", Rivew: "Actually I’m using this 4-5 days and its gave me effect. My stress level was so high but using this, its going low and I’m feeling light compare to before. I really recommend this capsule" },
        { id: 4, Ratings: 4.3, UserName: "Neha", RivewDate: "22/03/2020", Rivew: "From last few days I was suffering from insomnia. But thanks to this product, it gives quality sleep and boosting up my energy. A value for money product." }
      ],
      Quantity: 1
    };
  }

  renderReviews() {
    return this.state.ProductReviews.map((t) => {
      return (
        <View key={t.id} style={{ width: responsiveWidth(95), marginBottom: responsiveWidth(2) }}>
          <Text style={{ fontSize: responsiveFontSize(2), fontWeight: 'bold' }}>{t.UserName} </Text>
          <Rating
            type="star"
            fractions={1}
            imageSize={20}
            startingValue={t.Ratings}
            readonly
            style={{ alignSelf: 'flex-start' }} />
          <Text style={{ fontSize: responsiveFontSize(1), fontWeight: 'bold' }}>{t.RivewDate}</Text>
          <Text style={{ fontSize: responsiveFontSize(1.5) }}>{t.Rivew}</Text>
          <Text style={{ textAlign: 'center', fontSize: responsiveFontSize(1.5) }}>_________________________________________________________</Text>
        </View>
      )
    })
  }

  removeItem() {
    let num = this.state.Quantity
    if (num > 0) {
      num--
    }
    this.setState({ Quantity: num })
  }
  addItem() {
    let num = this.state.Quantity
    if (num >= 0) {
      num++
    }
    this.setState({ Quantity: num })
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
          <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
            <View style={{ flex: 0.7, flexDirection: 'row' }}>
              <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
              </TouchableOpacity>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}> {this.state.ProductName}</Text>
            </View>
            <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate("Cart") }}>
                <Icon name="shopping-cart" type="Feather" style={{ color: '#fff' }} />
              </TouchableOpacity>
            </View>
          </View>

        </View>
        <View style={{ flex: 0.91, backgroundColor: '#fff' }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
              <Image source={{ uri: this.state.ProductImge }} style={{ height: responsiveHeight(50), width: responsiveWidth(100) }} />
            </View>
            <View style={{ marginHorizontal: responsiveWidth(2), backgroundColor: '#fff' }}>
              <Text style={{ fontSize: responsiveFontSize(2.5), fontWeight: 'bold', marginTop: responsiveWidth(3), color: '#01AE52' }}>{this.state.ProductName}</Text>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate("ViewVaidhProfile")}}>
                <Text style={{fontSize:responsiveFontSize(1.3)}}> Product Seller : {this.state.ProductSeller}</Text>
              </TouchableOpacity>
              <Text style={{ fontSize: responsiveFontSize(2), fontWeight: 'bold', color: '#f11' }}>₹{this.state.ProductPrice}</Text>
              <View style={{ flexDirection: 'row' }}>
                <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(1.8) }}>Rating : </Text>
                <Rating type="star"
                  fractions={1}
                  imageSize={20}
                  startingValue={this.state.ProductRatings}
                  readonly />
                <Text style={{ fontSize: responsiveFontSize(1.5) }}>{this.state.ProductRatings}</Text>
              </View>
              <Text style={{ fontSize: responsiveFontSize(1.5) }}><Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(1.8) }}>Description : </Text>{this.state.ProductDescription}</Text>

            </View>

            <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), paddingHorizontal: responsiveWidth(2), backgroundColor: '#fff' }}>
              <Text style={{ fontWeight: 'bold', color: '#444', fontSize: responsiveFontSize(1.8) }}>Select Quantity : </Text>
              <TouchableOpacity style={{
                height: responsiveHeight(3),
                width: responsiveWidth(7),
                backgroundColor: '#777',
                borderTopLeftRadius: 10,
                borderBottomLeftRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}
                onPress={() => { this.removeItem() }}>
                <Icon name="md-remove" type="Ionicons" style={{ color: '#fff' }} />
              </TouchableOpacity>
              <Text style={{ fontWeight: 'bold', marginHorizontal: responsiveWidth(2) }}>{this.state.Quantity}</Text>

              <TouchableOpacity style={{
                height: responsiveHeight(3),
                width: responsiveWidth(7),
                backgroundColor: '#777',
                borderTopRightRadius: 10,
                borderBottomRightRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}
                onPress={() => { this.addItem() }}>
                <Icon name="md-add" type="Ionicons" style={{ color: '#fff' }} />
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(4) }}>
              <TouchableOpacity style={Styles.BorderLimeGreenButton}>
                <Text style={Styles.LimeGreenBoldText}>Add to Cart</Text>
              </TouchableOpacity>
              <TouchableOpacity style={Styles.FullLimeGreenButton}>
                <Text style={Styles.whiteBoldText}>Buy Now</Text>
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(6) }}>
              <View>
                {this.state.visibleWriteReviewModal && <WriteReview onExit={(val) => { this.setState({ visibleWriteReviewModal: val }) }} />}

                <TouchableOpacity style={{ flexDirection: 'row', height: responsiveHeight(5), width: responsiveWidth(90), borderTopWidth: 1, borderBottomWidth: 1, alignItems: 'center', }}
                  onPress={() => { this.setState({ visibleWriteReviewModal: true }) }}>
                  <View style={{ flex: 0.8 }}><Text>Write Review</Text></View>
                  <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                    <Icon name="ios-arrow-forward" type="Ionicons" />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(6) }}>
              <View>
                {this.renderReviews()}
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default ViewVaidhProduct;
