import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon, Card } from 'native-base';
import { Avatar } from 'react-native-elements';
import Styles from '../../../Components/Stylesheet/Styles';

class ViewVaidhProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            VaidhName: "Krishna Verma",
            Profile: "https://pbs.twimg.com/profile_images/537972841526734848/rdLjz1gO_400x400.jpeg ",
            Specialization: "Homeopathic",
            Description: "Hello these are just description lines just to test the font size and font color",
            VaidhAge: 42,
            VaidhFollowers: 1100,
            VaidhRatings: 4.3,
            Posts: 43,
            VaidhPosts: [
                { key: 1, PostDate: "12/03/2020", PostTime: "21:00", Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books", Likes: 32, Dislikes: 12 },
                { key: 2, PostDate: "09/03/2020", PostTime: "10:00", Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books", Likes: 51, Dislikes: 11 },
                { key: 3, PostDate: "05/03/2020", PostTime: "17:00", Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books", Likes: 35, Dislikes: 14 },
                { key: 4, PostDate: "02/03/2020", PostTime: "22:00", Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books", Likes: 44, Dislikes: 8 },
            ]
        };
    }

    renderPosts() {
        return this.state.VaidhPosts.map((t) => {
            return (
                <View key={t.key} style={{ width: responsiveWidth(100), marginVertical: responsiveWidth(1), alignItems: 'center', }}>
                    <Card style={{ width: responsiveWidth(100), padding: responsiveWidth(1.8), }}>
                        <View style={{ flexDirection: 'row', marginBottom: responsiveWidth(2) }}>
                            <View style={{ flex: 0.15 }}>
                                <Avatar source={{ uri: this.state.Profile }} size="medium" rounded={true} />
                            </View>
                            <View style={{ flex: 0.85 }}>
                                <Text style={{ fontWeight: 'bold' }}>{this.state.VaidhName}</Text>
                                <Text style={{ fontSize: responsiveFontSize(1), fontWeight: 'bold' }}>{t.PostDate} at {t.PostTime}</Text>
                            </View>
                        </View>
                        <Text style={{ fontSize: responsiveFontSize(1.5) }}>{t.Post}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}><Text style={{ fontSize: responsiveFontSize(1) }}>_______________________________________________________________________________</Text></View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity style={{ flexDirection: 'row', width: responsiveWidth(45), height: responsiveHeight(4), alignItems: 'center', justifyContent: 'center', }}>
                                <Icon name="like2" type="AntDesign" style={{ color: '#666' }} />
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(1.5), fontWeight: 'bold', marginLeft: responsiveWidth(1) }}>Like ({t.Likes})</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ flexDirection: 'row', width: responsiveWidth(45), height: responsiveHeight(4), alignItems: 'center', justifyContent: 'center', }}>
                                <Icon name="dislike2" type="AntDesign" style={{ color: '#666' }} />
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(1.5), fontWeight: 'bold', marginLeft: responsiveWidth(1) }}>Dislike ({t.Dislikes})</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </View>
            )
        })
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>{this.state.VaidhName}</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                            <TouchableOpacity>
                                <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{ flex: 0.91 }}>
                    <ScrollView>
                        <View style={{ flex: 0.22, flexDirection: 'row', padding: responsiveWidth(2) }}>
                            <View style={{ flex: 0.4,alignItems: 'center', }}>
                                <Avatar source={{ uri: this.state.Profile }} size="xlarge" rounded={true} />
                                <Text style={{ color: '#01AE52',fontWeight:'bold', fontSize: responsiveFontSize(1.6), }}>{this.state.Specialization}</Text>
                            </View>
                            <View style={{ flex: 0.6, justifyContent: 'center' }}>

                                <Text style={{ color: '#111', fontSize: responsiveFontSize(1.8), fontWeight: 'bold' }}>Description</Text>
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(1.4) }}>{this.state.Description}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(2) }}>

                                    <TouchableOpacity style={{
                                        height: responsiveHeight(4),
                                        width: responsiveWidth(25),
                                        borderColor: '#01AE52',
                                        borderWidth: 2,
                                        borderRadius: 10,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginHorizontal: responsiveWidth(3)
                                    }} onPress={() => { this.props.navigation.navigate("ViewVaidhStore") }}>
                                        <Text style={{ color: '#777', fontWeight: 'bold' }}>View Store</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{
                                        height: responsiveHeight(4),
                                        width: responsiveWidth(25),
                                        backgroundColor: '#01AE52',
                                        borderRadius: 10,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginHorizontal: responsiveWidth(3)
                                    }}>
                                        <Text style={Styles.whiteBoldText}>Follow</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginVertical: responsiveWidth(2) }}>
                            <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center', }}>
                                <Text style={{ fontSize: responsiveFontSize(2), fontWeight: 'bold', color: '#666' }}>POSTS</Text>
                                <Text style={{ textAlign: 'center' }}>{this.state.VaidhPosts.length}</Text>
                            </View>
                            <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center', }}>
                                <Text style={{ fontSize: responsiveFontSize(2), fontWeight: 'bold', color: '#666' }}>FOLLOWERS</Text>
                                <Text style={{ textAlign: 'center' }}>{this.state.VaidhFollowers}</Text>
                            </View>
                            <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center', }}>
                                <Text style={{ fontSize: responsiveFontSize(2), fontWeight: 'bold', color: '#666' }}>RATINGS</Text>
                                <Text style={{ textAlign: 'center' }}>{this.state.VaidhRatings}</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={{ alignItems: 'center', backgroundColor: '#3f3f3f', paddingVertical: responsiveWidth(3), marginHorizontal: responsiveWidth(2), borderRadius: 10 }}
                            onPress={() => { this.props.navigation.navigate("AddAppointment") }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold' }}>Request Appointment</Text>
                        </TouchableOpacity>

                        <View style={{ width: responsiveWidth(100), alignItems: 'center', }}>
                            {this.renderPosts()}
                        </View>

                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default ViewVaidhProfile;
