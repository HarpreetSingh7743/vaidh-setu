import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { responsiveFontSize, responsiveWidth } from 'react-native-responsive-dimensions';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';
import { Icon } from 'native-base';
import { Avatar } from 'react-native-elements';

class Nearby extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Locations: [
        { id: 1, Name: "Dr. Kanika Kapoor", AccountType: "Vaidh", Address: "Homopathic Clinic,Sector 38-C, Chandigarh", Latitude: 30.744434, longitude: 76.741412 },
        { id: 2, Name: "Dr. Akhil Aggarwal", AccountType: "YogaTeacher", Address: "Ayur Clinic,Sector 41-D, Chandigarh", Latitude: 30.732636, longitude: 76.736630 },
        { id: 3, Name: "Dr. Sumit Khanna", AccountType: "Vaidh", Address: "Patanjali Home, Sector 56, Chandigarh", Latitude: 30.736745, longitude: 76.717929 }
      ]
    };
  }

  AccountTypeManager(t) {
    if (t.AccountType == "Vaidh") {
      return (
        <Avatar source={{ uri: 'https://www.pinclipart.com/picdir/big/90-908744_ayurvedic-medicine-and-treatment-for-ayurveda-what-meditation.png' }} size="small" />
      )
    }
    else if (t.AccountType == "YogaTeacher") {
      return (
        <Avatar source={{ uri: 'https://www.iconsdb.com/icons/preview/royal-blue/yoga-xxl.png' }} size="small" />
      )
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
          <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
            <View style={{ flex: 0.7, flexDirection: 'row' }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Find Nearby</Text>
            </View>
            <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
              <TouchableOpacity>
                <Icon name="md-search" type="Ionicons" style={{ color: '#fff' }} />
              </TouchableOpacity>
            </View>
          </View>

        </View>
        <View style={{ flex: 0.91 }}>
          <MapView style={{ flex: 1 }}
            provider={PROVIDER_GOOGLE}
            region={{
              latitude: 30.743723,
              longitude: 76.729046,
              latitudeDelta: 0.0522,
              longitudeDelta: 0.0421,
            }}
            zoomEnabled={true}
            scrollEnabled={true}
            customMapStyle={[
              {
                "featureType": "administrative.land_parcel",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "administrative.neighborhood",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.attraction",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.business",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.business",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.government",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.government",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.medical",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.medical",
                "elementType": "geometry.fill",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.place_of_worship",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.school",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.sports_complex",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              }
            ]}>
            {
              this.state.Locations.map((item) => {
                return (
                  <Marker key={item.id}
                    coordinate={{ latitude: item.Latitude, longitude: item.longitude }}>
                    {this.AccountTypeManager(item)}
                    <Callout onPress={() => {
                      if (item.AccountType == "YogaTeacher") {
                        this.props.navigation.navigate("ViewTeacherProfile")
                      }
                      else if(item.AccountType=="Vaidh")
                      {
                        this.props.navigation.navigate("ViewVaidhProfile")
                      }
                    }} style={{ width: responsiveWidth(50) }}>
                      <Text style={{ color: '#111', fontWeight: 'bold' }}>{item.Name}</Text>
                      <Text>{item.Address}</Text>
                    </Callout>
                  </Marker>
                )
              })
            }
          </MapView>
        </View>
      </View>
    );
  }
}

export default Nearby;
