import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Radio } from 'native-base';
import { responsiveFontSize, responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import Styles from '../../../Components/Stylesheet/Styles';

class AddAppointment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            PatientName: "",
            PatientAge: "",
            Description: "",
            AppointmentTime: "",
            AppointmentDate: "",
            Radio1: true,
            Radio2: false,
            Timings: [
                { key: 1, Time: "09:00 AM", },
                { key: 2, Time: "09:30 AM", },
                { key: 3, Time: "10:00 AM", },
                { key: 4, Time: "10:30 AM", },
                { key: 5, Time: "11:00 AM", },
                { key: 6, Time: "12:00 PM", },
                { key: 7, Time: "12:30 PM", },
                { key: 8, Time: "01:00 PM", },
                { key: 9, Time: "01:30 PM", },
            ],
            checked: 0,
        };
    }

    componentDidMount() {
        let InitialTime = this.state.Timings[0].Time
        this.setState({ AppointmentTime: InitialTime })
    }
    todayDate() {

        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        var Today = date + "/" + month + "/" + year
        return (
            <Text style={{ fontWeight: 'bold', color: "#666" }}> ({Today})</Text>
        )
    }
    tommorowDate() {

        var date = new Date().getDate() + 1;
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        var Tommorow = date + "/" + month + "/" + year
        return (
            <Text style={{ fontWeight: 'bold', color: "#666" }}> ({Tommorow})</Text>
        )
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Request Appointment</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                            <TouchableOpacity>
                                <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 0.91 }}>
                    <ScrollView>
                        <Text style={{ color: '#01AE52', fontSize: responsiveWidth(5), fontWeight: 'bold', marginTop: responsiveWidth(3) }}>Enter Patient Details</Text>
                        <View style={{ width: responsiveWidth(100), paddingHorizontal: responsiveWidth(1.5), marginTop: responsiveWidth(3) }}>
                            <View style={{ marginBottom: responsiveWidth(3) }}>
                                <Text>Enter Patient Name *</Text>
                                <TextInput style={Styles.authInput}
                                    onChangeText={(PatientName) => { this.setState({ PatientName: PatientName }) }}
                                    onSubmitEditing={() => { this.refs.age.focus() }} />
                            </View>

                            <View style={{ marginBottom: responsiveWidth(3) }}>
                                <Text>Enter Patient Age *</Text>
                                <TextInput style={Styles.authInput}
                                    ref={'age'}
                                    onChangeText={(PatientAge) => { this.setState({ PatientAge: PatientAge }) }}
                                    onSubmitEditing={() => { this.refs.description.focus() }}
                                />
                            </View>

                            <View style={{ marginBottom: responsiveWidth(3) }}>
                                <Text>Describe your Problem</Text>
                                <TextInput style={{
                                    height: responsiveHeight(15),
                                    borderColor: '#01AE52',
                                    borderWidth: 1,
                                    paddingHorizontal: responsiveWidth(2),
                                    borderRadius: 10,
                                    marginBottom: responsiveWidth(2),
                                }}
                                    ref={'description'}
                                    onChangeText={(Description) => { this.setState({ Description: Description }) }}
                                    onSubmitEditing={() => { this.refs.age.focus() }}
                                    multiline />
                            </View>

                            <View style={{ marginBottom: responsiveWidth(3) }}>
                                <Text>Select Appointment Day *</Text>
                                <View style={{ flexDirection: 'row', marginTop: responsiveWidth(1) }}>
                                    <TouchableOpacity style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', }}
                                        onPress={() => { this.setState({ Radio1: true, Radio2: false, }) }}>
                                        <Radio selected={this.state.Radio1}
                                            onPress={() => { this.setState({ Radio1: true, Radio2: false }) }}
                                            selectedColor="#01AE52" />
                                        <Text>Today</Text>
                                        {this.todayDate()}
                                    </TouchableOpacity>

                                    <TouchableOpacity style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', }}
                                        onPress={() => { this.setState({ Radio2: true, Radio1: false }) }}>
                                        <Radio selected={this.state.Radio2}
                                            onPress={() => { this.setState({ Radio2: true, Radio1: false }) }}
                                            selectedColor="#01AE52" />
                                        <Text>Tommorow</Text>
                                        {this.tommorowDate()}
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Text>Select Appointment Time *</Text>
                            <View style={{ alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap', width: responsiveWidth(100) }}>

                                {this.state.Timings.map((t, key) => {
                                    return (
                                        <View key={t.key} style={{ marginRight: responsiveWidth(2), marginVertical: responsiveWidth(2) }}>
                                            {this.state.checked == key ?
                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', }}>
                                                    <Icon name="radio-button-checked" type="MaterialIcons" style={{ color: "#01AE52" }} />
                                                    <Text style={{ marginLeft: responsiveWidth(1) }}>{t.Time}</Text>
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', }} onPress={() => { this.setState({ checked: key, AppointmentTime: t.Time }) }}>
                                                    <Icon name="radio-button-unchecked" type="MaterialIcons" />
                                                    <Text style={{ marginLeft: responsiveWidth(1) }}>{t.Time}</Text>
                                                </TouchableOpacity>}
                                        </View>
                                    )
                                })}
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(4) }}>
                                <TouchableOpacity style={{
                                    width: responsiveWidth(90),
                                    paddingVertical: responsiveWidth(3),
                                    backgroundColor: '#01AE52',
                                    alignItems: 'center',
                                    borderRadius: 10
                                }}
                                    onPressIn={() => {
                                        var date = new Date().getDate();
                                        var month = new Date().getMonth() + 1;
                                        var year = new Date().getFullYear();
                                        var Today = date + "/" + month + "/" + year
                                        var Tommorow = date + 1 + "/" + month + "/" + year
                                        if (this.state.Radio1 == true) { this.setState({ AppointmentDate: Today }) }
                                        else { this.setState({ AppointmentDate: Tommorow }) }
                                    }}
                                    onPressOut={() => {

                                        alert(this.state.PatientName + "  " + this.state.PatientAge + "  " + this.state.Description + "  " + this.state.AppointmentTime + "  " + this.state.AppointmentDate)
                                    }}>
                                    <Text style={{ color: '#fff', fontWeight: 'bold' }}>Confirm</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default AddAppointment;
