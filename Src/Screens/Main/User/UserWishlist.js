import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon, Card } from 'native-base';
import { Avatar, Rating } from 'react-native-elements';
import Styles from '../../../Components/Stylesheet/Styles';

class UserWishlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      WishlistItems: [
        { key: 1, ProductName: "Cureveda Joint Care Oil", ProductRatings: 4.5, ProductImge: "https://cureveda.com/wp-content/uploads/2018/10/Joint-Care-Oil-Mock-Up-768x768.jpg", ProductPrice: 545 },
        { key: 2, ProductName: "Cureveda Lax Light", ProductRatings: 4.3, ProductImge: "https://cureveda.com/wp-content/uploads/Lax-Light-Tab-New-Mock-Up-768x768.jpg", ProductPrice: 395 },
        { key: 3, ProductName: "Cureveda Pureprash", ProductRatings: 4.2, ProductImge: "https://cureveda.com/wp-content/uploads/2019/01/Pureprash-Mock-UP-768x768.jpg", ProductPrice: 600 },
        { key: 4, ProductName: "Cureveda Gluco Guide", ProductRatings: 4.3, ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Gluco-1-768x768.jpg", ProductPrice: 520 },
        { key: 5, ProductName: "Cureveda Sleep Sure", ProductRatings: 4.1, ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Sleep-Sure-1-768x768.jpg", ProductPrice: 450 },
        { key: 6, ProductName: "Cureveda Brain Boost", ProductRatings: 3.9, ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Brain-Boost-1-768x768.jpg", ProductPrice: 560 },
      ],
    };
  }

  renderWishlist() {
    return this.state.WishlistItems.map((t) => {
      let ProductImge = t.ProductImge
      return (
        <View key={t.key} style={{ width: responsiveWidth(100), alignItems: 'center', }}>
          <Card style={{ width: responsiveWidth(99), paddingVertical: responsiveWidth(2) }}>
            <TouchableOpacity style={{ flexDirection: 'row', paddingHorizontal: responsiveWidth(1) }} onPress={()=>{this.props.navigation.navigate("ViewVaidhProduct")}}>
              <View style={{ flex: 0.25 }}>
                <Avatar source={{ uri: ProductImge }} size="large" rounded />
              </View>
              <View style={{ flex: 0.75 }}>
                <Text style={{ fontWeight: 'bold' }}>{t.ProductName}</Text>
                <Rating
                  type="star"
                  fractions={1}
                  imageSize={17}
                  startingValue={t.ProductRatings}
                  readonly
                  style={{ alignSelf: 'flex-start' }} />
                <Text> ₹{t.ProductPrice}</Text>
              </View>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', paddingHorizontal: responsiveWidth(3), marginTop: responsiveWidth(4), alignItems: 'center' }}>
              <TouchableOpacity style={{
                flex: 0.5,
                alignItems: 'center',
                borderWidth: 0.4,
                borderTopLeftRadius: 15,
                borderBottomLeftRadius: 15,
                borderColor: '#777',
                paddingVertical: responsiveWidth(1.5),
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row'
              }}>
                <Icon name="trash" type="FontAwesome5" style={{ fontSize: responsiveFontSize(1.5), color: '#666', }} />
                <Text style={{ fontSize: responsiveFontSize(1.5), color: '#666', marginLeft: responsiveWidth(1) }}>Remove</Text>

              </TouchableOpacity>

              <TouchableOpacity style={{
                flex: 0.5,
                alignItems: 'center',
                borderWidth: 0.4,
                borderTopRightRadius: 15,
                borderBottomRightRadius: 15,
                borderColor: '#777',
                paddingVertical: responsiveWidth(1.5),
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row'
              }}>
                 <Icon name="shopping-cart" type="Feather" style={{ fontSize: responsiveFontSize(1.5), color: '#01AE52' }} />
                <Text style={{ fontSize: responsiveFontSize(1.5), color: '#666', marginLeft: responsiveWidth(1) }}>Add to Cart</Text>
              </TouchableOpacity>

            </View>
          </Card>
        </View>
      )
    })
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
          <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
            <View style={{ flex: 0.7, flexDirection: 'row' }}>
              <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
              </TouchableOpacity>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Wishlist</Text>
            </View>
            <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
              <TouchableOpacity>
                <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{ color: '#fff' }} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={{ flex: 0.91 }}>
          <ScrollView>
            {this.renderWishlist()}
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default UserWishlist;
