import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { Card, Icon } from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Avatar, Rating } from 'react-native-elements';
import Styles from '../../../Components/Stylesheet/Styles';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

class AllYogaTeachers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            yogaTeachers: [
                { key: 1, TeacherName: "Rajan Kumar", Profile: "https://www.citrix.com/blogs/wp-content/uploads/2018/03/slack_compressed-e1521621363404-360x360.jpg", TeacherAge: 34, TeacherFollowers: 1250, TeacherRatings: 4.5, Posts: 12 },
                { key: 2, TeacherName: "Rakesh Kumar", Profile: "https://likewise-stage.azureedge.net/uploads/3eb6cf23-895b-45e9-b92c-5fb1b457dd04/bill-gates-profile-pic.jpg", TeacherAge: 52, TeacherFollowers: 1200, TeacherRatings: 4.5, Posts: 33 },
                { key: 3, TeacherName: "Tarun Verma", Profile: "https://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg", TeacherAge: 42, TeacherFollowers: 1100, TeacherRatings: 4.3, Posts: 43 },
                { key: 4, TeacherName: "Robin Singh", Profile: "https://www.pngfind.com/pngs/m/317-3177131_636682202684372240-user-profile-photo-round-hd-png-download.png", TeacherAge: 44, TeacherFollowers: 990, TeacherRatings: 4.3, Posts: 41 },
                { key: 5, TeacherName: "Kirpal Singh", Profile: "https://tunnelwell.com/wp-content/uploads/2016/07/team-member-5.jpg", TeacherAge: 32, TeacherFollowers: 1503, TeacherRatings: 4.3, Posts: 155 },
                { key: 6, TeacherName: "Ashish Kumar", Profile: "https://i.imgur.com/WByph2V.jpg", TeacherAge: 23, TeacherFollowers: 1899, TeacherRatings: 4.2, Posts: 122 },
                { key: 7, TeacherName: "Ranjan Verma", Profile: "https://i.imgur.com/JFHjdNr.jpg", TeacherAge: 27, TeacherFollowers: 1987, TeacherRatings: 4.1, Posts: 19 },
                { key: 8, TeacherName: "Jaspreet Kaur", Profile: "https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-3/img/user-04.3ac7ddd3.jpg", TeacherAge: 44, TeacherFollowers: 2103, TeacherRatings: 3.9, Posts: 90 },
                { key: 9, TeacherName: "Sushmita Singh", Profile: "https://pixinvent.com/materialize-material-design-admin-template/app-assets/images/user/12.jpg", TeacherAge: 31, TeacherFollowers: 993, TeacherRatings: 3.2, Posts: 31 }
            ]
        };
    }

    RenderYogaTeachers() {
        return this.state.yogaTeachers.map((t) => {
            let profile = t.Profile
            return (
                <TouchableOpacity key={t.key} style={{ width: responsiveWidth(95), flexDirection: 'row', borderRadius: 15 }} onPress={()=>{this.props.navigation.navigate("ViewTeacherProfile")}}>
                    <View style={{ flex: 0.2, alignItems: 'center', justifyContent: 'center' }}>
                        <Avatar source={{ uri: profile }} size="large" rounded />
                    </View>
                    <View style={{ flex: 0.8, padding: responsiveWidth(2), flexDirection: 'row' }}>
                        <View style={{ flex: 0.9 }}>
                            <Text style={{ color: '#01AE52', fontSize: responsiveFontSize(2), fontWeight: 'bold' }}>{t.TeacherName}</Text>
                            <Text style={{ color: '#666', fontWeight: 'bold', fontSize: responsiveFontSize(1.3) }}>Followers: {t.TeacherFollowers}</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(1.3) }}>{t.TeacherRatings}</Text>
                                <Rating type="star"
                                    fractions={1}
                                    imageSize={20}
                                    startingValue={t.TeacherRatings}
                                    readonly
                                    style={{ alignSelf: 'flex-start' }} />

                            </View>
                        </View>
                        <View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', }}>
                            <Icon name="ios-arrow-forward" type="Ionicons" style={{ fontSize: responsiveFontSize(1.8) }} />
                        </View>

                    </View>
                </TouchableOpacity>
            )
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Yoga Teachers</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                            <TouchableOpacity>
                                <Icon name="search" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{ flex: 0.05, alignItems: 'center', justifyContent: 'center', flexDirection: 'row',backgroundColor:'#fff' }}>

                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginRight: responsiveWidth(3) }}>
                            <Icon name="filter" type="Feather" />
                            <Text style={{ fontSize: responsiveFontSize(1.5), fontWeight: 'bold' }}>Filter</Text>
                        </View>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Popularity</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Rated 3.5+</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Distance</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Popularity</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Distance</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Popularity</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
                <View style={{ flex: 0.86, alignItems: 'center',backgroundColor:'#fff' }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {this.RenderYogaTeachers()}
                    </ScrollView>
                </View>

            </View>
        );
    }
}

export default AllYogaTeachers;

const styles = {
    filterButton: {
        backgroundColor: '#01AE52',
        padding: responsiveWidth(1.3),
        borderRadius: 20,
        marginHorizontal: responsiveWidth(1.5)
    },
    filterText: {
        color: "#fff"
    }
}