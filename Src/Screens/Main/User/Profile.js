import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { Icon, Card, CardItem } from 'native-base';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import Styles from '../../../Components/Stylesheet/Styles';
import { Avatar } from 'react-native-elements';
import EditProfileModal from '../../../Components/Modals/User/EditProfileModal';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      VisibleEditProfileModal:false,
      AvatarSource: "https://pixinvent.com/materialize-material-design-admin-template/app-assets/images/user/12.jpg",
      UserName: "Jenifer Tracy",
      UserAge: "22",
      UserContact: "9876543210",
      UserAddress: "House No. 1234,Sector 11, Chandigarh",
      State: "Chandigarh",
      City: "Chandigarh",
      PinCode: 198879,
      OrderHistory: [
        {
          key: 0,
          ItemSeller: "Naresh Kumar",
          OrderId: "#1234",
          OrderedItem: "Cureveda Joint Care Oil",
          OrderedItemImage: "https://cureveda.com/wp-content/uploads/2018/10/Joint-Care-Oil-Mock-Up-768x768.jpg",
          Quantity: "2",
          OrderPrice: "330",
          OrderDate: "12/05/2020",
          Status: "Undelivered",
          DeliveryDate: ""
        },
        {
          key: 1,
          ItemSeller: "Ranjan Verma",
          OrderId: "#8989",
          OrderedItem: "Cureveda Lax Light",
          OrderedItemImage: "https://cureveda.com/wp-content/uploads/Lax-Light-Tab-New-Mock-Up-768x768.jpg",
          Quantity: "2",
          OrderPrice: "430",
          OrderDate: "10/05/2020",
          Status: "Undelivered",
          DeliveryDate: ""
        },
        {
          key: 2,
          ItemSeller: "Robin Singh",
          OrderId: "#4543",
          OrderedItem: "Cureveda Pureprash",
          OrderedItemImage: "https://cureveda.com/wp-content/uploads/2019/01/Pureprash-Mock-UP-768x768.jpg",
          Quantity: "1",
          OrderPrice: "550",
          OrderDate: "06/05/2020",
          Status: "Delivered",
          DeliveryDate: "08/05/2020"
        },
        {
          key: 3,
          ItemSeller: "Robin Singh",
          OrderId: "#4456",
          OrderedItem: "Cureveda Brain Boost",
          OrderedItemImage: "https://cureveda.com/wp-content/uploads/2020/02/Brain-Boost-1-768x768.jpg",
          Quantity: "1",
          OrderPrice: "325",
          OrderDate: "03/05/2020",
          Status: "Delivered",
          DeliveryDate: "05/05/2020"
        },
        {
          key: 4,
          ItemSeller: "Krishna Verma",
          OrderId: "#7667",
          OrderedItem: "Cureveda Brain Boost",
          OrderedItemImage: "https://cureveda.com/wp-content/uploads/2020/02/Brain-Boost-1-768x768.jpg",
          Quantity: "4",
          OrderPrice: "990",
          OrderDate: "14/04/2020",
          Status: "Delivered",
          DeliveryDate: "18/04/2020"
        },
      ],
      following: [
        {
          key: 1,
          Name: "Naresh Kumar",
          AccountType:"Vaidh",
          Profile: "https://www.illomayurveda.com/img/illom-about2.jpg",
          Ratings: 4.5,
        },
        {
          key: 2,
          Name: "Vaibhav Kumar",
          AccountType:"Vaidh",
          Profile: "https://www.illomayurveda.com/img/Eshwara-Sarma.jpg",
          Ratings: 4.5,
        },
        {
          key: 3,
          Name: "Krishna Verma",
          AccountType:"Vaidh",
          Profile: "https://pbs.twimg.com/profile_images/537972841526734848/rdLjz1gO_400x400.jpeg",
          Ratings: 4.3,
        },
        {
          key: 4,
          Name: "Robin Singh",
          AccountType:"Vaidh",
          Profile: "https://www.pngfind.com/pngs/m/317-3177131_636682202684372240-user-profile-photo-round-hd-png-download.png",
          Ratings: 4.3,
        },
        {
          key: 5,
          Name: "Kirpal Singh",
          AccountType:"YogaTeacher",
          Profile: "https://tunnelwell.com/wp-content/uploads/2016/07/team-member-5.jpg",
          Ratings: 4.3,
        },
        {
          key: 6,
          Name: "Ashish Kumar",
          AccountType:"YogaTeacher",
          Profile: "https://i.imgur.com/WByph2V.jpg",
          Ratings: 4.2,
        },
        {
          key: 7,
          Name: "Ranjan Verma",
          AccountType:"YogaTeacher",
          Profile: "https://i.imgur.com/JFHjdNr.jpg",
          Ratings: 4.1,
        },
      ]
    };
  }

  renderOrderHistory() {
    return this.state.OrderHistory.map((t) => {
      let ProductImage = t.OrderedItemImage
      return (
        <View key={t.key} style={{ width: responsiveWidth(95), paddingVertical: responsiveWidth(2), marginVertical: responsiveWidth(2) }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.25 }}>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate("ViewVaidhProduct") }}>
                <Avatar source={{ uri: ProductImage }} size="large" />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 0.75 }}>
              <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(1.6) }}>{t.OrderedItem}</Text>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate("ViewVaidhProfile") }}><Text style={{ fontSize: responsiveFontSize(1.3) }}>Seller : <Text style={{ fontSize: responsiveFontSize(1.3), color: '#55f' }}>{t.ItemSeller}</Text> </Text></TouchableOpacity>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Order Id : {t.OrderId}</Text>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Price : {t.OrderPrice}</Text>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Quantity : {t.Quantity}</Text>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Order Date : {t.OrderDate}</Text>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Status : {t.Status} </Text>
              <Text style={{ fontSize: responsiveFontSize(1.3) }}>Delivered on : {t.Status == "Delivered" ? t.DeliveryDate : ""}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', paddingHorizontal: responsiveWidth(3), marginTop: responsiveWidth(4), alignItems: 'center' }}>
            <TouchableOpacity style={{
              flex: 0.5,
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderBottomLeftRadius: 15,
              borderWidth: 0.4,
              borderColor: '#777',
              paddingVertical: responsiveWidth(1.5),
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row'
            }}>
              <Icon name="heart" type="FontAwesome" style={{ fontSize: responsiveFontSize(1.5), color: '#f11' }} />
              <Text style={{ fontSize: responsiveFontSize(1.5), color: '#666', marginLeft: responsiveWidth(1) }}>Add to Wishlist</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{
              flex: 0.5,
              alignItems: 'center',
              borderTopRightRadius: 15,
              borderBottomRightRadius: 15,
              borderWidth: 0.4,
              borderColor: '#777',
              paddingVertical: responsiveWidth(1.5),
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row'
            }}>
              {t.Status == "Delivered" ? <Icon name="reload1" type="AntDesign" style={{ fontSize: responsiveFontSize(1.5), color: '#666', }} /> : <Icon name="close" type="AntDesign" style={{ fontSize: responsiveFontSize(1.8), color: '#666', }} />}

              <Text style={{ fontSize: responsiveFontSize(1.5), color: '#666', marginLeft: responsiveWidth(1) }}>{t.Status == "Delivered" ? "Order Again" : "Cancel Order"}</Text>
            </TouchableOpacity>

          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Text style={{ fontSize: responsiveFontSize(1.3), color: '#888' }}>___________________________________________________________________</Text>
          </View>
        </View>
      )
    })
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
          <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
            <View style={{ flex: 0.7, flexDirection: 'row' }}>
              <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Profile</Text>
            </View>
            <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
            </View>
          </View>

        </View>
        <View style={{ flex: 0.91 }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 0.4, padding: responsiveWidth(1), }}>
                <Avatar source={{ uri: this.state.AvatarSource }} showAccessory size="xlarge" rounded={true} onAccessoryPress={() => { alert("Insert ImagePicker here") }} />
              </View>
              <View style={{ flex: 0.6, justifyContent: 'center', }}>
                <Text style={{ color: '#01AE52', fontSize: responsiveWidth(6), fontWeight: 'bold' }}>{this.state.UserName}</Text>
                <Text style={{ fontSize: responsiveWidth(3), fontWeight: 'bold' }}>+91 {this.state.UserContact}</Text>

                <TouchableOpacity style={{ marginTop: responsiveWidth(8) }} onPress={() => { this.props.navigation.navigate("UserFollowingPeoples",{following:this.state.following}) }}>
                  <Text style={{ fontWeight: 'bold', color: "#888" }}>FOLLOWING : {this.state.following.length} Peoples</Text>
                </TouchableOpacity>
              </View>
            </View>
            <Card style={{ flex: 0.4, padding: responsiveWidth(2) }}>
             <View style={{flexDirection:'row',alignItems:'center'}}>
             <Text style={{ color: '#01AE52', fontSize: responsiveWidth(5), fontWeight: 'bold' }}>About</Text>


            {this.state.VisibleEditProfileModal && <EditProfileModal onExit={(val)=>{this.setState({VisibleEditProfileModal:val})}}/>}

             <TouchableOpacity onPress={()=>{this.setState({VisibleEditProfileModal:true})}}>
               <Icon name="edit" type="Feather" style={{fontSize:responsiveFontSize(2.3),color: '#01AE52',marginLeft:responsiveWidth(3)}}/>
             </TouchableOpacity>
             </View>
              <View style={{ marginTop: responsiveWidth(2) }}>
                <Text style={{ fontWeight: 'bold' }}>Age         : {this.state.UserAge}</Text>
                <Text style={{ fontWeight: 'bold' }}>Address : {this.state.UserAddress}</Text>
                <Text style={{ fontWeight: 'bold' }}>PinCode : {this.state.PinCode}</Text>
                <Text style={{ fontWeight: 'bold' }}>State       : {this.state.State}</Text>
                <Text style={{ fontWeight: 'bold' }}>City          : {this.state.City}</Text>
              </View>
            </Card>
            <Card style={{ flex: 0.4, padding: responsiveWidth(2) }}>
              <Text style={{ color: '#01AE52', fontSize: responsiveWidth(4), fontWeight: 'bold' }}>Manage Orders</Text>
              <View style={{ marginTop: responsiveWidth(2) }}>
                {this.renderOrderHistory()}
              </View>
            </Card>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default Profile;
