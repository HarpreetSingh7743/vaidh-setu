import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, ScrollView } from 'react-native';
import { Card, Icon } from 'native-base';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Avatar, Rating } from 'react-native-elements';

class UserFollowingPeoples extends Component {
    constructor(props) {
        super(props);
        this.state = {
            following: this.props.navigation.state.params.following,
            VisibleModal: true
        };
    }

    goToProfile(t) {
        if (t.AccountType == "Vaidh") {
            this.props.navigation.navigate("ViewVaidhProfile")
        }
        else if (t.AccountType == "YogaTeacher") {
            this.props.navigation.navigate("ViewTeacherProfile")
        }
    }

    renderFollowing() {
        return this.state.following.map((t) => {
            let ProfileImage = t.Profile
            return (
                <View key={t.key} style={{ width: responsiveWidth(100), marginVertical: responsiveWidth(2) }}>
                    <View style={{ flexDirection: 'row',paddingHorizontal:responsiveWidth(1) }}>
                        <View style={{ flex: 0.15 }}>
                            <TouchableOpacity style={{ flex: 0.17, borderRadius: 10 }} onPress={() => { this.goToProfile(t) }}>
                                <Avatar source={{ uri: ProfileImage }} rounded size="medium" />
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 0.45 }}>

                            <Text style={{ fontSize: responsiveFontSize(1.4) }}>{t.Name}</Text>
                            <Rating
                                type="star"
                                fractions={1}
                                imageSize={10}
                                startingValue={t.Ratings}
                                readonly
                                style={{ alignSelf: 'flex-start' }} />
                        </View>

                        <TouchableOpacity style={{ flex: 0.4, alignItems: 'center', justifyContent: 'center', backgroundColor: '#01AE52', borderRadius: 10,margin:responsiveWidth(1.5) }}>
                            <Text style={{ fontSize: responsiveFontSize(1.3), color: '#fff' }}>Following</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )

        })
    }
    render() {
        return (

            <View style={{ flex: 1, }}>
                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7, flexDirection: 'row', alignItems: 'center', }}>
                            <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Following</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                        </View>
                    </View>

                </View>
                <View style={{ flex: 0.91,backgroundColor:'#fff' }}>
                    <ScrollView>
                        {this.renderFollowing()}
                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default UserFollowingPeoples;
