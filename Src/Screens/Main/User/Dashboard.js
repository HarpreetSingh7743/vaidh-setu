import React, { Component } from 'react';
import { View, Text, ImageBackground, Dimensions, SafeAreaView, TouchableOpacity, TextInput, TouchableWithoutFeedback, ScrollView, TouchableHighlight, Image } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon, Card } from 'native-base';
import Styles from '../../../Components/Stylesheet/Styles';
class Dashboard extends Component {
    constructor(props) {
        super(props);
        // Sandeep
        this.state = {
            UserLocation: "Sector 41 C, Chandigarh",
            QuickServices: [
                { key: 1, ServiceName: "Top Rated Vaids", Image: "https://ayurvedicdietplans.com/assets/images/slide2m.jpg" },
                { key: 2, ServiceName: "Top Rated Yoga Teachers", Image: "https://studiogrowth.com/wp-content/uploads/2020/03/Make-money-online-as-a-yoga-teacher.jpg" },
                { key: 3, ServiceName: "Most liked Herbs", Image: "https://www.thestatesman.com/wp-content/uploads/2018/11/Herbs.jpg" }
            ],
            MainServices: [
                { key: 1, ServiceName: "Yoga Teacher", ServiceIcon: "user-graduate", ServiceIconType: "FontAwesome5", Path: "AllYogaTeachers" },
                { key: 2, ServiceName: "Nearby Vaidh", ServiceIcon: "user-md", ServiceIconType: "FontAwesome5", Path: "AllVaidhs" },
                { key: 3, ServiceName: "Ayurvedic Meds", ServiceIcon: "medkit", ServiceIconType: "FontAwesome", Path: "AllProducts" },
                { key: 4, ServiceName: "Appointments", ServiceIcon: "chat", ServiceIconType: "MaterialIcons", Path: "Appointments" },
                { key: 5, ServiceName: "Wishlist", ServiceIcon: "heart", ServiceIconType: "FontAwesome5", Path: "UserWishlist" }
            ],
            Tip: " Brush your teeth, gargle with lukewarm water and put a few drops of oil in each nostril every morning.This will clear your senses and prepare you for the day ahead "
        };
    }

    renderQuickServices() {
        
        return this.state.QuickServices.map((t) => {
            let Image=t.Image
            return (
                <TouchableHighlight key={t.key} style={{ marginHorizontal: responsiveWidth(2) }} >
                    <Card style={{ width: responsiveWidth(80), height: responsiveHeight(22), marginHorizontal: 10, borderRadius: 15 }}>
                       <ImageBackground source={{uri:Image}} style={{width: responsiveWidth(80), height: responsiveHeight(22)}}>

                       </ImageBackground>
                    </Card>
                </TouchableHighlight>
            )
        })
    }

    renderMainServices() {
        return this.state.MainServices.map((t) => {
            return (
                <View key={t.key} style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{
                        width: responsiveWidth(18), height: responsiveHeight(9.5), backgroundColor: '#01AE52',
                        marginHorizontal: responsiveWidth(5), marginVertical: responsiveWidth(1), borderRadius: 40,
                        alignItems: 'center', justifyContent: 'center'
                    }}>
                        <TouchableOpacity style={{ height: responsiveHeight(9.5), width: responsiveWidth(18), alignItems: 'center', justifyContent: 'center' }}  onPress={() => { this.props.navigation.navigate(t.Path) }}>
                            <Icon name={t.ServiceIcon} type={t.ServiceIconType} style={{ color: '#fff' }} />
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(1.5), color: '#666' }}>{t.ServiceName}</Text>
                </View>
            )
        })
    }

    render() {
        return (
            <ImageBackground source={require("../../../Components/Images/BG/MainBack.jpg")} style={{ flex: 1 }}>
                <View style={{ flex: 0.1, flexDirection: 'row', justifyContent: 'center', paddingTop: responsiveWidth(8), paddingHorizontal: responsiveWidth(2.5) }}>
                    <View style={{ flex: 0.8 }}>
                        <TouchableWithoutFeedback onPress={() => { alert("Your Location : " + this.state.UserLocation) }}>
                            <Text style={{ color: "#fff", fontSize: responsiveFontSize(2.5), fontWeight: 'bold' }}>{this.state.UserLocation}</Text>
                        </TouchableWithoutFeedback>
                        <TouchableOpacity>
                            <Text style={Styles.whiteBoldText}>(Change)</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                        <TouchableOpacity onPress={()=>{this.props.navigation.navigate("Cart")}}>
                            <Icon name="shopping-cart" type="Feather" style={{ color: '#fff', fontSize: responsiveFontSize(4) }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center', }}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        {this.renderQuickServices()}
                    </ScrollView>
                </View>
                <View style={{ flex: 0.4, }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ flexDirection: 'row', width: responsiveWidth(100), flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center' }}>
                            {this.renderMainServices()}
                        </View>
                    </ScrollView>
                </View>
                <View style={{ flex: 0.2, alignItems: 'center', paddingTop: responsiveWidth(2) }}>
                    <Text style={{ fontSize: responsiveFontSize(2), color: '#01AE52', fontWeight: 'bold', textAlign: 'center' }}>Tip of the day</Text>
                    <Text style={{ width: responsiveWidth(80), fontWeight: 'bold', textAlign: 'center' }}> "{this.state.Tip}"</Text>
                </View>
            </ImageBackground>
        );
    }
}

export default Dashboard;
