import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { responsiveFontSize, responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon, Card } from 'native-base';
import { Avatar } from 'react-native-elements';
import Styles from '../../../Components/Stylesheet/Styles';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

class AllBlogs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            AllBlogs: [
                {
                    key: 1,
                    AuthorName: "Naveen Kumar",
                    AccountType:"Vaidh",
                    Profile: "https://www.citrix.com/blogs/wp-content/uploads/2018/03/slack_compressed-e1521621363404-360x360.jpg",
                    PostDate: "12/03/2020",
                    PostTime: "21:00",
                    Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books",
                    Likes: 32,
                    Dislikes: 12
                },
                {
                    key: 2,
                    AuthorName: "Sunil Singh",
                    AccountType:"Vaidh",
                    Profile: "https://likewise-stage.azureedge.net/uploads/3eb6cf23-895b-45e9-b92c-5fb1b457dd04/bill-gates-profile-pic.jpg",
                    PostDate: "09/03/2020",
                    PostTime: "10:00",
                    Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books",
                    Likes: 51,
                    Dislikes: 11
                },
                {
                    key: 3,
                    AuthorName: "Harshita Kaur",
                    AccountType:"YogaTeacher",
                    Profile: "https://pixinvent.com/materialize-material-design-admin-template/app-assets/images/user/12.jpg",
                    PostDate: "05/03/2020",
                    PostTime: "17:00",
                    Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books",
                    Likes: 35,
                    Dislikes: 14
                },
                {
                    key: 4,
                    AuthorName: "Ranjan Verma",
                    AccountType:"YogaTeacher",
                    Profile: "https://i.imgur.com/JFHjdNr.jpg",
                    PostDate: "02/03/2020",
                    PostTime: "22:00",
                    Post: "Status allows you to share text, photo, video and GIF updates that disappear after 24 hours. In order to send and receive status updates to and from your contacts, you and your contacts must have each other’s phone numbers saved in your phones’ address books",
                    Likes: 44,
                    Dislikes: 8
                },

            ]
        };
    }

    goToProfile(t){
        if(t.AccountType=="Vaidh"){
            this.props.navigation.navigate("ViewVaidhProfile")
        }
        else if(t.AccountType=="YogaTeacher"){
            this.props.navigation.navigate("ViewTeacherProfile")
        }
    }

    renderBlogs(){
        return this.state.AllBlogs.map((t) => {
           let ProfileImage=t.Profile
            return (
                <View key={t.key} style={{ width: responsiveWidth(100), marginVertical: responsiveWidth(1), alignItems: 'center', }}>
                    <Card style={{ width: responsiveWidth(100), padding: responsiveWidth(1.8) }}>
                        <TouchableWithoutFeedback style={{ flexDirection: 'row', marginBottom: responsiveWidth(2) }} onPress={()=>{this.goToProfile(t)}}>
                            <View style={{ flex: 0.15 }}>
                                <Avatar source={{ uri: ProfileImage }} size="medium" rounded={true} />
                            </View>
                            <View style={{ flex: 0.85 }}>
                                <Text style={{ fontWeight: 'bold' }}>{t.AuthorName}</Text>
                                <Text style={{ fontSize: responsiveFontSize(1), fontWeight: 'bold' }}>{t.PostDate} at {t.PostTime}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <Text style={{ fontSize: responsiveFontSize(1.5) }}>{t.Post}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}><Text style={{ fontSize: responsiveFontSize(1),color:'#999' }}>_______________________________________________________________________________</Text></View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity style={{ flexDirection: 'row', width: responsiveWidth(45), height: responsiveHeight(4), alignItems: 'center', justifyContent: 'center', }}>
                                <Icon name="like2" type="AntDesign" style={{ color: '#666' }} />
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(1.5), fontWeight: 'bold', marginLeft: responsiveWidth(1) }}>Like ({t.Likes})</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ flexDirection: 'row', width: responsiveWidth(45), height: responsiveHeight(4), alignItems: 'center', justifyContent: 'center', }}>
                                <Icon name="dislike2" type="AntDesign" style={{ color: '#666' }} />
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(1.5), fontWeight: 'bold', marginLeft: responsiveWidth(1) }}>Dislike ({t.Dislikes})</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </View>
            )
        })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7, flexDirection: 'row' }}>
                            
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Blogs</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                            <TouchableOpacity>
                                <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{flex:0.91,alignItems: 'center',backgroundColor:'#fff'}}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {this.renderBlogs()}
                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default AllBlogs;
