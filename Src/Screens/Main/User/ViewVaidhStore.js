import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon, Card } from 'native-base';
import { Avatar, Rating } from 'react-native-elements';
import Styles from '../../../Components/Stylesheet/Styles';

class ViewVaidhStore extends Component {
    constructor(props) {
        super(props);
        this.state = {
            StoreItems: [
                { key: 1, ProductName: "Cureveda Joint Care Oil", ProductRatings: 4.5, ProductImge: "https://cureveda.com/wp-content/uploads/2018/10/Joint-Care-Oil-Mock-Up-768x768.jpg", ProductPrice: 545 },
                { key: 2, ProductName: "Cureveda Lax Light", ProductRatings: 4.3, ProductImge: "https://cureveda.com/wp-content/uploads/Lax-Light-Tab-New-Mock-Up-768x768.jpg", ProductPrice: 395 },
                { key: 3, ProductName: "Cureveda Pureprash", ProductRatings: 4.2, ProductImge: "https://cureveda.com/wp-content/uploads/2019/01/Pureprash-Mock-UP-768x768.jpg", ProductPrice: 600 },
                { key: 4, ProductName: "Cureveda Gluco Guide", ProductRatings: 4.3, ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Gluco-1-768x768.jpg", ProductPrice: 520 },
                { key: 5, ProductName: "Cureveda Sleep Sure", ProductRatings: 4.1, ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Sleep-Sure-1-768x768.jpg", ProductPrice: 450 },
                { key: 6, ProductName: "Cureveda Brain Boost", ProductRatings: 3.9, ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Brain-Boost-1-768x768.jpg", ProductPrice: 560 },
            ],
            
        };
    }

    renderStoreItems() {
        return this.state.StoreItems.map((t) => {
            let ProductImge = t.ProductImge
            return (
                <View key={t.key} style={{ width: responsiveWidth(45), marginHorizontal: responsiveWidth(2), marginVertical: responsiveWidth(3), alignItems: 'center', justifyContent: 'center', }}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate("ViewVaidhProduct")}}>
                        <Avatar source={{ uri: ProductImge }} size="xlarge" />
                        <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(1.5) }}>{t.ProductName}</Text>
                        <Text style={{ fontSize: responsiveFontSize(1.3) }}>Price :  ₹{t.ProductPrice}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontSize: responsiveFontSize(1.3) }}>Ratings : </Text>
                            <Rating type="star"
                                fractions={1}
                                imageSize={15}
                                startingValue={t.ProductRatings}
                                readonly
                                style={{ alignSelf: 'flex-start' }} />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity style={{
                        flexDirection: 'row',
                        height: responsiveHeight(5),
                        width: responsiveWidth(35),
                        backgroundColor: '#01AE52',
                        marginTop: responsiveWidth(2),
                        borderRadius: 10,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <Icon name="shopping-cart" type="Feather" style={{ color: '#fff' }} />
                        <Text style={Styles.whiteBoldText}> Add to Cart</Text>
                    </TouchableOpacity>
                </View>
            )

        })
    }


    render() {
        return (
            <View style={{ flex: 1,backgroundColor:'#fff' }}>
                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Store</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                            <TouchableOpacity  onPress={()=>{this.props.navigation.navigate("Cart")}}>
                                <Icon name="shopping-cart" type="Feather" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{ flex: 0.91, alignItems: 'center', justifyContent: 'center', }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{
                            flexDirection: 'row',
                            width: responsiveWidth(100),
                            flexWrap: "wrap",
                            alignItems: 'center', justifyContent: 'center',
                        }}>
                            {this.renderStoreItems()}
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default ViewVaidhStore;
