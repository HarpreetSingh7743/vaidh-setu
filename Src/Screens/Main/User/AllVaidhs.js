import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { Card, Icon } from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Avatar, Rating } from 'react-native-elements';
import Styles from '../../../Components/Stylesheet/Styles';

class AllVaidhs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Vaidhs: [
                { key: 1, VaidhName: "Naresh Kumar", Profile: "https://www.illomayurveda.com/img/illom-about2.jpg", VaidhAge: 34, VaidhFollowers: 1250, VaidhRatings: 4.5,  },
                { key: 2, VaidhName: "Vaibhav Kumar", Profile: "https://www.illomayurveda.com/img/Eshwara-Sarma.jpg", VaidhAge: 52, VaidhFollowers: 1200, VaidhRatings: 4.5,  },
                { key: 3, VaidhName: "Krishna Verma", Profile: "https://pbs.twimg.com/profile_images/537972841526734848/rdLjz1gO_400x400.jpeg", VaidhAge: 42, VaidhFollowers: 1100, VaidhRatings: 4.3,  },
                { key: 4, VaidhName: "Robin Singh", Profile: "https://www.pngfind.com/pngs/m/317-3177131_636682202684372240-user-profile-photo-round-hd-png-download.png", VaidhAge: 44, VaidhFollowers: 990, VaidhRatings: 4.3,  },
                { key: 5, VaidhName: "Kirpal Singh", Profile: "https://tunnelwell.com/wp-content/uploads/2016/07/team-member-5.jpg", VaidhAge: 32, VaidhFollowers: 1503, VaidhRatings: 4.3,  },
                { key: 6, VaidhName: "Ashish Kumar", Profile: "https://i.imgur.com/WByph2V.jpg", VaidhAge: 23, VaidhFollowers: 1899, VaidhRatings: 4.2, },
                { key: 7, VaidhName: "Ranjan Verma", Profile: "https://i.imgur.com/JFHjdNr.jpg", VaidhAge: 27, VaidhFollowers: 1987, VaidhRatings: 4.1,  },
            ]
        };
    }

    RenderVaidhs() {
        return this.state.Vaidhs.map((t) => {
            let profile = t.Profile
            return (
                <TouchableOpacity key={t.key} style={{ width: responsiveWidth(95), flexDirection: 'row', borderRadius: 15,marginBottom:responsiveWidth(3) }} onPress={()=>{this.props.navigation.navigate("ViewVaidhProfile")}}>
                    <View style={{ flex: 0.2,alignItems: 'center', justifyContent: 'center' }}>
                        <Avatar source={{ uri: profile }} size="large" rounded={true} />
                    </View>
                    <View style={{ flex: 0.8, padding: responsiveWidth(2),flexDirection:'row' }}>
                        <View style={{ flex: 0.9 }}>
                            <Text style={{ color: '#01AE52', fontSize: responsiveFontSize(2), fontWeight: 'bold' }}>{t.VaidhName}</Text>
                            <Text style={{ color: '#666', fontWeight: 'bold', fontSize: responsiveFontSize(1.3) }}>Followers: {t.VaidhFollowers}</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(1.3) }} >Ratings:</Text>
                                <Rating type="star"
                                    fractions={1}
                                    imageSize={17}
                                    startingValue={t.VaidhRatings}
                                    readonly
                                    style={{ alignSelf: 'flex-start' }} />
                            </View>
                        </View>
                        <View style={{flex:0.1,alignItems: 'center',justifyContent: 'center',}}>
                            <Icon name="ios-arrow-forward" type="Ionicons" style={{fontSize:responsiveFontSize(1.8)}}/>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 0.09, backgroundColor: '#01AE52' }}>
                    <View style={{ flexDirection: 'row', marginTop: responsiveWidth(8), marginHorizontal: responsiveWidth(3) }}>
                        <View style={{ flex: 0.7, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon name="md-arrow-back" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Vaidhs</Text>
                        </View>
                        <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                            <TouchableOpacity>
                                <Icon name="search" type="Ionicons" style={{ color: '#fff' }} />
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
                <View style={{ flex: 0.05, alignItems: 'center', justifyContent: 'center', flexDirection: 'row',backgroundColor:'#fff' }}>

                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginRight: responsiveWidth(3) }}>
                            <Icon name="filter" type="Feather" />
                            <Text style={{ fontSize: responsiveFontSize(1.5) }}>Filter</Text>
                        </View>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Popularity</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Rated 3.5+</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={Styles.whiteBoldText}>Distance</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Homeopathic</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Chiropratic</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.filterButton}>
                            <Text style={styles.filterText}>Popularity</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
                <View style={{ flex: 0.86, alignItems: 'center',backgroundColor:'#fff' }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {this.RenderVaidhs()}
                    </ScrollView>
                </View>

            </View>
        );
    }
}

export default AllVaidhs;

const styles = {
    filterButton: {
        backgroundColor: '#01AE52',
        padding: responsiveWidth(1),
        borderRadius: 20,
        marginHorizontal: responsiveWidth(1.5)
    },
    filterText:{
        color:"#fff"
    }
}