import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, TextInput } from 'react-native';
import { Card, Icon, Item, Label, Input, NativeBase, Picker } from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import Styles from '../../Stylesheet/Styles';

class ChangeDeliveryAddressModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: true,
            Cities: [
                { label: "Andhra Pradesh", value: "key2" },
                { label: "Arunachal Pradesh", value: "key3" },
                { label: "Assam", value: "key4" },
                { label: "Bihar", value: "key5" },
                { label: "Chandigarh", value: "key6" },
                { label: "Chhattisgarh", value: "key7" },
                { label: "Delhi", value: "key8" },
                { label: "Goa", value: "key9" },
                { label: "Gujarat", value: "key10" },
                { label: "Haryana", value: "key11" },
                { label: "Himachal Pradesh", value: "key12" },
                { label: "Jammu and Kashmir", value: "key13" },
                { label: "Jharkhand", value: "key14" },
                { label: "Karnataka", value: "key15" },
                { label: "Kerala", value: "key16" },
                { label: "Lakshadweep", value: "key17" },
                { label: "Madhya Pradesh", value: "key18" },
                { label: "Maharashtra", value: "key19" },
                { label: "Manipur", value: "key20" },
                { label: "Meghalaya", value: "key21" },
                { label: "Mizoram", value: "key22" },
                { label: "Nagaland", value: "key23" },
                { label: "Orissa", value: "key24" },
                { label: "Puducherry", value: "key25" },
                { label: "Punjab", value: "key26" },
                { label: "Rajasthan", value: "key27" },
                { label: "Sikkim", value: "key28" },
                { label: "Tamil Nadu", value: "key29" },
                { label: "Telangana", value: "key30" },
                { label: "Tripura", value: "key31" },
                { label: "Uttar Pradesh", value: "key32" },
                { label: "Uttarakhand", value: "key33" },
                { label: "West Bengal", value: "key34" },
            ],
            DeliveryAddress:"",
            ZIPcode:"",
            selectedState: "key0",
            selectedCity: "key0",
            State: "",
            City: "",
            savedAddress: [
                {}, {}
            ]
        };
    }

    renderCities() {
        return this.state.Cities.map((t) => {
            return (
                <Picker.Item key={t.value} label={t.label} value={t.value} />
            )
        })
    }

    onValueChange = (value = String, label) => {
        this.setState({
            selectedState: value,
            State: label
        });
    }
    onValueChange1 = (value = String, label) => {
        this.setState({
            selectedCity: value,
            City: label
        });
    }

    render() {
        return (
            <Modal transparent={true} visible={this.state.visibleModal} animationType="slide" onRequestClose={() => { this.props.onExit(false) }}>
                <View style={{ flex: 1, backgroundColor: 'rgba(1,1,1,0.5)', alignItems: 'center', justifyContent: 'flex-end', }}>
                    <Card style={{ width: responsiveWidth(99), height: responsiveHeight(50) }}>
                        <View style={{ flexDirection: 'row', paddingHorizontal: responsiveWidth(2), marginTop: responsiveWidth(2) }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(2) }}>Select Delivery Address</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.onExit(false) }}>
                                    <Icon name="close" type="MaterialCommunityIcons" />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ marginTop: responsiveWidth(6), paddingHorizontal: responsiveWidth(3) }}>
                            <TouchableOpacity style={{ flexDirection: 'row', marginBottom: responsiveWidth(3), paddingVertical: responsiveWidth(1), alignItems: 'center', }}>
                                <Icon name="my-location" type="MaterialIcons" style={{ color: '#01AE52' }} />
                                <Text>  Use my Location</Text>
                            </TouchableOpacity>

                            <Text style={{ fontSize: responsiveFontSize(1.3) }}>Enter full Address</Text>
                            <TextInput style={Styles.authInput}
                                placeholder="Enter address"
                                keyboardType="default" 
                                onChangeText={(DeliveryAddress)=>{this.setState({DeliveryAddress:DeliveryAddress})}}/>

                            <Text style={{ fontSize: responsiveFontSize(1.3) }}>Enter Area Code</Text>
                            <TextInput style={Styles.authInput}
                                placeholder="Area Code"
                                keyboardType="number-pad" 
                                onChangeText={(ZIPcode)=>{this.setState({ZIPcode:ZIPcode})}}/>

                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 0.5, }}>
                                    <Text style={{ fontSize: responsiveFontSize(1.3) }}>Select State</Text>
                                    <View style={{ width: responsiveWidth(40), height: responsiveHeight(5), borderWidth: 1, borderColor: '#01AE52', borderRadius: 10, alignItems: 'center', justifyContent: 'center', }}>
                                        <Picker
                                            note
                                            mode="dropdown"
                                            placeholder="Select State"
                                            style={{ width: responsiveWidth(40) }}
                                            selectedValue={this.state.selectedState}
                                            onValueChange={this.onValueChange.bind(this)}
                                        >
                                            {this.renderCities()}
                                        </Picker>
                                    </View></View>
                                <View style={{ flex: 0.5, }}>
                                    <Text style={{ fontSize: responsiveFontSize(1.3) }}>Select City</Text>
                                    <View style={{ width: responsiveWidth(40), height: responsiveHeight(5), borderWidth: 1, borderColor: '#01AE52', borderRadius: 10, alignItems: 'center', justifyContent: 'center', }}>
                                        <Picker
                                            note
                                            mode="dropdown"
                                            placeholder="Select State"
                                            style={{ width: responsiveWidth(40) }}
                                            selectedValue={this.state.selectedCity}
                                            onValueChange={this.onValueChange1.bind(this)}
                                        >
                                            {this.renderCities()}
                                        </Picker>
                                    </View>
                                </View>
                            </View>

                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(6) }}>
                            <TouchableOpacity style={{ width: responsiveWidth(90), paddingVertical: responsiveWidth(3), backgroundColor: '#01AE52', alignItems: 'center', borderRadius: 10 }}
                            onPress={()=>{alert("Address : "+this.state.DeliveryAddress+" ZIP code: " +this.state.ZIPcode+" State: "+this.state.Cities[this.state.State].label+" City: " + this.state.Cities[this.state.City].label)}}>
                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </View>
            </Modal>
        );
    }
}

export default ChangeDeliveryAddressModal;
