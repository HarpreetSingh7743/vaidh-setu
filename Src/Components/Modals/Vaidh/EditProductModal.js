import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image, Modal, Alert } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon, Card } from 'native-base';
import { Avatar, Rating } from 'react-native-elements';
import Styles from '../../../Components/Stylesheet/Styles';
import { TextInput } from 'react-native-gesture-handler';

class EditProductModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ProductName: "Cureveda Sleep Sure",
            ProductDescription: "For deep & calm sleep support Sleep quality, deep sleep in sleepless nights & insomnia, Time to fall asleep, regularizing sleep cycle and keeping the mind relaxed, Duration of sleep, Promotes tranquility , calming effect & relaxation, Valerian root, Sarpagandha, Jatamansi, 30 vegetarian tablets, 15-30 day pack, 100% herbal, safe for long term use, no side effects",
            ProductRatings: 4.5,
            ProductImge: "https://cureveda.com/wp-content/uploads/2020/02/Sleep-Sure-1-768x768.jpg",
            ProductPrice: "545",
            WarehouseQuantity:"105",
            ProductReviews: [
                { id: 1, Ratings: 2.7, UserName: "Shubham", RivewDate: "12/04/2020", Rivew: "Because of my work I need to travel from one place to another and I am not getting enough sleep. This tablet is very helpful for me to get enough sleep and feel relaxed." },
                { id: 2, Ratings: 4.1, UserName: "Seema", RivewDate: "09/04/2020", Rivew: "Overall These medicine Improved My Sleep. It also Helped in Reducing My Stress. These Tablets are Very High Quality and Works Effectively to Most of The People. Overall a Very Good Tablets" },
                { id: 3, Ratings: 4.3, UserName: "Yash", RivewDate: "01/04/2020", Rivew: "Actually I’m using this 4-5 days and its gave me effect. My stress level was so high but using this, its going low and I’m feeling light compare to before. I really recommend this capsule" },
                { id: 4, Ratings: 4.3, UserName: "Neha", RivewDate: "22/03/2020", Rivew: "From last few days I was suffering from insomnia. But thanks to this product, it gives quality sleep and boosting up my energy. A value for money product." }
            ],
        };
    }


    renderReviews() {
        return this.state.ProductReviews.map((t) => {
            return (
                <View key={t.id} style={{ width: responsiveWidth(95), marginBottom: responsiveWidth(5) }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ fontSize: responsiveFontSize(2), fontWeight: 'bold' }}>{t.UserName} </Text>
                        <Rating
                            type="star"
                            fractions={1}
                            imageSize={20}
                            startingValue={t.Ratings}
                            readonly
                            style={{ alignSelf: 'flex-start' }} />
                    </View>
                    <Text style={{ fontSize: responsiveFontSize(1), fontWeight: 'bold' }}>{t.RivewDate}</Text>
                    <Text style={{ fontSize: responsiveFontSize(1.5) }}>{t.Rivew}</Text>
                </View>
            )
        })
    }
    render() {
        return (
            <Modal visible={true} animationType="slide" onRequestClose={()=>{this.props.onExit(false)}} >

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ backgroundColor: '#01AE52', height: responsiveHeight(8), flexDirection: 'row', paddingHorizontal: responsiveWidth(2), alignItems: 'center', }}>
                        <View style={{ flex: 0.5, }}>
                            <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: responsiveFontSize(2), marginLeft: responsiveWidth(3) }}>Edit Product Detais</Text>
                        </View>
                        <View style={{ flex: 0.5, flexDirection: 'row', justifyContent: 'flex-end', }}>
                            <TouchableOpacity style={{
                                height: responsiveHeight(4.5),
                                width: responsiveWidth(17),
                                borderWidth: 2,
                                borderColor: "#fff",
                                alignItems: 'center',
                                justifyContent: 'center',
                                marginRight: responsiveWidth(1.5),
                                borderRadius: 15
                            }} onPress={()=>{this.props.onExit(false)}}>
                                <Text style={{ color: '#fff' }}>Cancel</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{
                                borderRadius: 15,
                                marginLeft: responsiveWidth(1.5),
                                height: responsiveHeight(4.5),
                                width: responsiveWidth(17),
                                backgroundColor: '#fff',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <Text style={{ color: "#01AE52" }}>Save</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                        <Image source={{ uri: this.state.ProductImge }} style={{ height: responsiveHeight(50), width: responsiveWidth(100) }} />
                    </View>
                    <View style={{ marginHorizontal: responsiveWidth(2), backgroundColor: '#fff' }}>
                        <Text style={{ fontSize: responsiveFontSize(2.5), fontWeight: 'bold', marginTop: responsiveWidth(3), color: '#01AE52' }}>{this.state.ProductName}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(1.8)}}>Price : </Text>
                            <TextInput style={{ fontSize: responsiveFontSize(2), fontWeight: 'bold', color: '#f11', borderBottomWidth: 1, width: responsiveWidth(20) }} 
                            value={this.state.ProductPrice}
                            keyboardType="number-pad"
                            onChangeText={(ProductPrice)=>{this.setState({ProductPrice:ProductPrice})}}/>
                        </View>
                       
                        <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(1.8) }}>Warehouse Quantity : </Text>
                                <TextInput style={{ fontSize: responsiveFontSize(2), fontWeight: 'bold', color: '#111', borderBottomWidth: 1, width: responsiveWidth(20) }} 
                            value={this.state.WarehouseQuantity}
                            keyboardType="number-pad"
                            onChangeText={(WarehouseQuantity)=>{this.setState({WarehouseQuantity:WarehouseQuantity})}}/>
                            </View>
                            <View style={{ marginTop: responsiveWidth(1.5) }}>
                                <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(1.8) }}>Description : </Text>
                                <TextInput style={{ fontSize: responsiveFontSize(1.5), height: responsiveHeight(15), width: responsiveWidth(95), borderWidth: 1, paddingHorizontal: 5, borderRadius: 10 }}
                                    value={this.state.ProductDescription}
                                    onChangeText={(ProductDescription) => { this.setState({ ProductDescription: ProductDescription }) }}
                                    multiline />
                            </View>
                        </View>

                        <View style={{}}>
                            <Text style={{ fontSize: responsiveFontSize(3), marginTop: responsiveWidth(5), marginLeft: responsiveWidth(2) }}>Peoples Review</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                            <View>
                                {this.renderReviews()}
                            </View>
                        </View>
                </ScrollView>
            </Modal>
        );
    }
}

export default EditProductModal;
