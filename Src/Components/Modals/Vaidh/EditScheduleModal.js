import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, TextInput } from 'react-native';
import { responsiveWidth, responsiveFontSize, responsiveHeight } from 'react-native-responsive-dimensions';
import { Icon, CheckBox } from 'native-base';
import Styles from '../../Stylesheet/Styles';

class EditScheduleModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { id: 1, key: "Sunday", checked: false },
        { id: 2, key: "Monday", checked: false },
        { id: 3, key: "Tueday", checked: false },
        { id: 4, key: "Wednesday", checked: false },
        { id: 5, key: "Thursday", checked: false },
        { id: 6, key: "Friday", checked: false },
        { id: 7, key: "Saturday", checked: false },
      ],
      openTime: "",
      CloseTime: "",
      WeekDays: []
    };
  }

  handleClick = async () => {
    var res1 = this.state.data.map((t) => t.key)
    var res2 = this.state.data.map((t) => t.checked)
    var resultArray = []
    for (let i = 0; i < res2.length; i++) {
      if (res2[i] == true) {
        resultArray.push(res1[i])
      }
    }
    await this.setState({ WeekDays: resultArray })
    console.log(this.state.WeekDays);
  }

  onCheckChanged(id) {
    const data = this.state.data;
    const index = data.findIndex(x => x.id === id);
    data[index].checked = !data[index].checked;
    this.setState(data);
  }
  renderDays() {
    return this.state.data.map((item, key) => {
      return (
        <TouchableOpacity key={key} style={{ flexDirection: 'row', marginBottom: responsiveWidth(2),marginLeft:responsiveWidth(2) }} onPress={() => this.onCheckChanged(item.id)}>
          <View style={{ flex: 0.1 }}>
            <CheckBox checked={item.checked} onPress={() => this.onCheckChanged(item.id)} color="#01AE52" style={{borderRadius:5}}/>
          </View>
          <View style={{ flex: 0.8 }}>
            <Text allowFontScaling={false}>{item.key}</Text>
          </View>
        </TouchableOpacity>)
    })
  }
  render() {
    return (
      <Modal visible={true} transparent={true} onRequestClose={() => { this.props.onExit(false) }}>
        <View style={{ flex: 1, justifyContent: 'flex-end', backgroundColor: 'rgba(1,1,1,0.4)' }}>
          <View style={{ flex: 0.6, backgroundColor: '#fff' }}>
            <View style={{ flex: 0.08, flexDirection: 'row', paddingHorizontal: responsiveWidth(2), marginTop: responsiveWidth(2) }}>
              <View style={{ flex: 0.8 }}>
                <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(2) }}>Edit Schedule</Text>
              </View>
              <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                <TouchableOpacity onPress={() => { this.props.onExit(false) }}>
                  <Icon name="close" type="MaterialCommunityIcons" />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ flex: 0.8, paddingHorizontal: responsiveWidth(2) }}>
              <Text style={{color:'#01AE52', fontWeight:'bold', fontSize:responsiveFontSize(1.8),marginBottom:responsiveWidth(2)}}> Select Open Days : </Text>
              {this.renderDays()}

              <Text style={{color:'#01AE52', fontWeight:'bold', fontSize:responsiveFontSize(1.8),marginTop:responsiveWidth(4),}}> Select Open Time : </Text>
              
              <View style={{flexDirection:'row',marginTop:responsiveWidth(4)}}>
                <View style={{flex:0.45,paddingRight:responsiveWidth(2)}}>
                  <TextInput style={Styles.authInput}
                  placeholder="HH:MM AM/PM"
                  onSubmitEditing={()=>{this.refs.next.focus()}}
                  onChangeText={(openTime)=>{this.setState({openTime:openTime})}}/>
                </View>
                <Text>to</Text>
                <View style={{flex:0.45,paddingLeft:responsiveWidth(2)}}>
                  <TextInput style={Styles.authInput}
                  placeholder="HH:MM AM/PM"
                  onChangeText={(CloseTime)=>{this.setState({CloseTime:CloseTime})}}
                  ref={'next'}/>
                </View>
              </View>
            </View>

            <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(4) }}>
              <TouchableOpacity style={{ width: responsiveWidth(95), paddingVertical: responsiveWidth(3), backgroundColor: '#01AE52', alignItems: 'center', borderRadius: 10 }}
                onPress={() => {
                  this.handleClick()
                }}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Confirm</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

export default EditScheduleModal;
