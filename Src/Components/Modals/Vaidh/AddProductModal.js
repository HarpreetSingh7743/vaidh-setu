import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, TextInput } from 'react-native';
import { Card, Icon, Item, Label, Input, NativeBase, Picker } from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import Styles from '../../Stylesheet/Styles';
import { Avatar } from 'react-native-elements';

class AddProductModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: true,
            ProductName:"",
            ProductDescription:"",
            ProductPrice:"",
            WarehouseQuantity:""
        };
    }

    render() {
        return (
            <Modal transparent={true} visible={this.state.visibleModal} animationType="slide" onRequestClose={() => { this.props.onExit(false) }}>
                <View style={{ flex: 1, backgroundColor: 'rgba(1,1,1,0.5)', alignItems: 'center', justifyContent: 'flex-end', }}>
                    <Card style={{ width: responsiveWidth(99), height: responsiveHeight(47) }}>
                        <View style={{ flexDirection: 'row', paddingHorizontal: responsiveWidth(2), marginTop: responsiveWidth(2) }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(2) }}>Enter Product Details</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.onExit(false) }}>
                                    <Icon name="close" type="MaterialCommunityIcons" />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 0.7 }}>
                                <View style={{ marginTop: responsiveWidth(6), paddingHorizontal: responsiveWidth(3) }}>
                                    <TextInput style={Styles.authInput}
                                        placeholder="Product Name"
                                        keyboardType="default"
                                        onSubmitEditing={()=>{this.refs.Desc.focus()}}
                                        onChangeText={(ProductName)=>{this.setState({ProductName:ProductName})}} />

                                    <TextInput style={{
                                        height: responsiveHeight(10),
                                        borderColor: '#01AE52',
                                        borderWidth: 1,
                                        paddingHorizontal: responsiveWidth(2),
                                        borderRadius: 10,
                                        marginBottom: responsiveWidth(2),
                                    }}
                                        multiline
                                        placeholder="Description"
                                        ref={'Desc'}
                                        keyboardType="default" 
                                        onChangeText={(ProductDescription)=>{this.setState({ProductDescription:ProductDescription})}}
                                        />

                                    <View style={{width:responsiveWidth(40)}}>
                                    <TextInput style={Styles.authInput}
                                        placeholder="Enter Price"
                                        keyboardType="number-pad" 
                                        onChangeText={(ProductPrice)=>{this.setState({ProductPrice:ProductPrice})}}
                                        onSubmitEditing={()=>{this.refs.quantity.focus()}}/>

                                    <TextInput style={Styles.authInput}
                                        placeholder="Warehouse Quantity"
                                        keyboardType="number-pad" 
                                        ref={'quantity'}
                                        onChangeText={(WarehouseQuantity)=>{this.setState({WarehouseQuantity:WarehouseQuantity})}}/>
                                    </View>
                                </View>
                            </View>
                            <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center', }}>
                                <TouchableOpacity style={{ borderWidth: 0.5, paddingHorizontal: responsiveWidth(1.5), paddingVertical: responsiveWidth(3), alignItems: 'center', borderRadius: 15 }}>
                                    <Avatar source={require('../../Images/Icons/Buttons/AddProduct.png')} size="medium" />
                                    <Text>Add Image</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(4) }}>
                            <TouchableOpacity style={{ width: responsiveWidth(90), paddingVertical: responsiveWidth(3), backgroundColor: '#01AE52', alignItems: 'center', borderRadius: 10 }}>
                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </View>
            </Modal>
        );
    }
}

export default AddProductModal;
