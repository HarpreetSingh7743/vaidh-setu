import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, TextInput, Alert } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Icon } from 'native-base';
import Styles from '../../Stylesheet/Styles';

class AddVideoModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <Modal transparent={true} visible={true} animationType="fade">
                <View style={{ flex: 1, justifyContent: 'flex-end', backgroundColor: 'rgba(1,1,1,0.4)' }}>
                    <View style={{ flex: 0.4, backgroundColor: '#fff', borderTopLeftRadius: 15, borderTopRightRadius: 15, paddingHorizontal: responsiveWidth(2), paddingTop: responsiveWidth(1) }}>
                        <View style={{ flexDirection: 'row', paddingHorizontal: responsiveWidth(2), marginTop: responsiveWidth(2) }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(2) }}>Enter Video details</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.onExit(false) }}>
                                    <Icon name="close" type="MaterialCommunityIcons" />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ marginTop: responsiveWidth(2), alignItems: 'center', }}>
                            <View style={{ width: responsiveWidth(90) }}>
                                <View style={{ width: responsiveWidth(50) }}>
                                    <TextInput style={Styles.authInput}
                                        placeholder="Enter YouTube Video ID"
                                        onSubmitEditing={() => { this.refs.title.focus() }} />
                                </View>
                                <TextInput style={Styles.authInput}
                                    placeholder="Enter Video Title"
                                    ref={'title'}
                                    onSubmitEditing={() => { this.refs.description.focus() }}
                                />
                                <TextInput style={{
                                    height: responsiveHeight(10),
                                    borderColor: '#01AE52',
                                    borderWidth: 1,
                                    paddingHorizontal: responsiveWidth(2),
                                    borderRadius: 10,
                                    marginBottom: responsiveWidth(2),
                                }}
                                    multiline
                                    placeholder="Video Description"
                                    ref={'description'}
                                    onSubmitEditing={() => { alert("Done") }} />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveWidth(4) }}>
                            <TouchableOpacity style={{ width: responsiveWidth(90), paddingVertical: responsiveWidth(3), backgroundColor: '#01AE52', alignItems: 'center', borderRadius: 10 }}>
                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

export default AddVideoModal;
