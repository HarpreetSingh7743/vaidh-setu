import React, { Component } from 'react';
import { View, Text, Modal, TextInput, TouchableOpacity } from 'react-native';
import { Card, Icon } from 'native-base';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Rating } from 'react-native-elements';

class WriteReview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleModal: true,
            UserRating: 2.5,
            UserReview:""
        };
    }

    render() {
        return (
            <Modal visible={this.state.visibleModal} animationType="slide" transparent={true} onRequestClose={() => { this.props.onExit(false) }}>
                <View style={{ flex: 1, backgroundColor: 'rgba(1,1,1,0.3)', alignItems: 'center', justifyContent: 'center', }}>
                    <Card style={{ height: responsiveHeight(60), width: responsiveWidth(90), borderRadius: 10, padding: responsiveWidth(2) }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={{ fontSize: responsiveFontSize(2.5), fontWeight: 'bold' }}>Write a Review</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.onExit(false) }}>
                                    <Icon name="close" type="AntDesign" />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <Rating
                            fractions={1}
                            
                            onFinishRating={(rating) => { this.setState({ UserRating: rating }) }}
                        />
                        <View style={{ flex: 0.85 }}>
                            <TextInput style={{ width: responsiveWidth(85), }} 
                            multiline={true} 
                            placeholder="Write your Rivew here" 
                            onChangeText={(UserReview)=>{this.setState({UserReview:UserReview})}}/>
                        </View>
                        <View style={{ flex: 0.14, alignItems: 'center', justifyContent: 'center', }}>
                            <TouchableOpacity style={{
                                width: responsiveWidth(85),
                                borderWidth: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                                paddingVertical: responsiveWidth(3),
                                borderRadius: 10,
                                borderColor: '#01AE52'
                            }} 
                            onPress={()=>{alert("Rating : "+this.state.UserRating+"            Message : "+this.state.UserReview)}}>
                                <Text style={{ color: '#01AE52', fontWeight: 'bold' }}>Post Review</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </View>
            </Modal>
        );
    }
}

export default WriteReview;
