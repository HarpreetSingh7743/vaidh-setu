import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity } from 'react-native';
import { Card, Icon } from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { TextInput } from 'react-native-gesture-handler';
import Styles from '../Stylesheet/Styles';

class EnterOTPModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            UserMobile: this.props.UserMobile,
            visibleModal: true,
            one: "",
            two: "",
            three: "",
            four: "",
            otp: this.props.OTP
        };
    }

    render() {
        let OTP = parseInt(this.state.one + this.state.two + this.state.three + this.state.four)

        return (
            <Modal transparent={true} animationType="fade" onRequestClose={() => { this.props.onExit(false) }}>
                <View style={{ flex: 1, backgroundColor: 'rgba(1,1,1,0.3)', alignItems: 'center', justifyContent: 'center', }}>
                    <Card style={{ width: responsiveWidth(80), height: responsiveHeight(30), borderRadius: 15, padding: responsiveWidth(3) }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
                            <View style={{ flex: 0.8 }}>
                                <Text style={{ color: '#666', fontSize: responsiveFontSize(3), fontWeight: 'bold' }}>Enter OTP</Text>
                            </View>
                            <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => { this.props.onExit(false) }}>
                                    <Icon name="close" type="MaterialIcons" />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <Text style={{ color: '#888' }}>Type the OTP sent to {this.state.UserMobile}</Text>
                        <View style={{ marginVertical: responsiveWidth(7), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    maxLength={1}
                                    onChangeText={(one) => { this.setState({ one: one }), this.refs.two.focus() }} />
                            </View>

                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    maxLength={1}
                                    onChangeText={(two) => { this.setState({ two: two }), this.refs.three.focus() }}
                                    ref={'two'} />
                            </View>

                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    maxLength={1}
                                    onChangeText={(three) => { this.setState({ three: three }), this.refs.four.focus() }}
                                    ref={'three'} />
                            </View>

                            <View style={styles.OtpBox}>
                                <TextInput style={styles.OtpInput}
                                    keyboardType="number-pad"
                                    maxLength={1}
                                    onChangeText={(four) => { this.setState({ four: four }) }}
                                    ref={'four'} />
                            </View>
                        </View>

                        <View style={{ width: responsiveWidth(75), alignItems: 'center', }}>
                            <TouchableOpacity style={Styles.BorderLimeGreenButton} onPress={() => {
                                if (this.state.otp == OTP) { this.props.onSubmit(false) }
                                else { alert("Wrong OTP") }
                            }}>
                                <Text style={Styles.LimeGreenBoldText}>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>
                </View>
            </Modal>
        );
    }
}

export default EnterOTPModal;

const styles = {
    OtpBox: {
        borderWidth: 1,
        height: responsiveHeight(5),
        width: responsiveWidth(10),
        marginHorizontal: responsiveWidth(2.5),
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    OtpInput: {
        width: responsiveWidth(4),
        fontSize: responsiveFontSize(2.5)
    }
}