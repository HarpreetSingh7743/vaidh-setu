import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, TextInput } from 'react-native';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Icon } from 'native-base';

class WritePostModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      VisibleModal: true
    };
  }

  render() {
    return (
      <Modal visible={this.state.VisibleModal} transparent={true} animationType="slide" onRequestClose={()=>{ this.props.onExit(false) }}>
        <View style={{ flex: 1, justifyContent: 'flex-end', backgroundColor: 'rgba(1,1,1,0.4)' }}>
          <View style={{ width: responsiveWidth(100), height: responsiveHeight(30), backgroundColor: '#fff', borderTopLeftRadius: 15, borderTopRightRadius: 15 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 0.7 }}>
                <TouchableOpacity style={{ width: responsiveWidth(10), height: responsiveHeight(6), justifyContent: 'center', alignItems: 'center', }}
                  onPress={() => { this.props.onExit(false) }}>
                  <Icon name="close" type="AntDesign" />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                <TouchableOpacity style={{ width: responsiveWidth(15), height: responsiveHeight(6), justifyContent: 'center' }}>
                  <Text style={{ fontSize: responsiveFontSize(2),color:'#01AE52',fontWeight:'bold' }}>Post</Text>
                </TouchableOpacity>
              </View>
            </View>
            <TextInput
              style={{
                width: responsiveWidth(95),
                height: responsiveHeight(20),
                borderWidth: 0.5,
                borderRadius: 20,
                padding: responsiveWidth(2),
                marginVertical: responsiveWidth(1),
                alignSelf:'center'
              }} 
              multiline
              placeholder="Write Something here."/>
          </View>
        </View>
      </Modal>
    );
  }
}

export default WritePostModal;
