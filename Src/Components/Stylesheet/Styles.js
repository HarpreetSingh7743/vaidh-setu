import { StyleSheet } from 'react-native'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions'

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    logoStyle:{
        height:responsiveHeight(10),
        width:responsiveWidth(25)
    },

    authInput: {
        height: responsiveHeight(5),
        borderColor:'#01AE52',
        borderWidth:1,
        paddingHorizontal: responsiveWidth(2),
        borderRadius: 10,
        marginBottom: responsiveWidth(2),
    },

    greenBoldText:{
        color:'#098630',
        fontWeight:'bold'
    },

    whiteBoldText:{
        color:'#fff',
        fontWeight:'bold'
    },

    whiteHeaderText:{
        fontSize:responsiveFontSize(4),
        color:'#fff',
        fontWeight:'bold'
    },

    LimeGreenBoldText:{
        color:'#01AE52',
        fontWeight:'bold',
    },

    LimeGreenNormalText:{
        color:'#01AE52'
    },

    FullLimeGreenButton:{
        height:responsiveHeight(5),
        width:responsiveWidth(35),
        backgroundColor:'#01AE52',
        borderRadius:10,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal:responsiveWidth(5)
    },

    BorderLimeGreenButton:{
        height:responsiveHeight(5),
        width:responsiveWidth(35),
        borderColor:'#01AE52',
        borderWidth:1,
        borderRadius:10,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal:responsiveWidth(5)
    },

    fullWhiteButton:{
        height:responsiveHeight(5),
        width:responsiveWidth(35),
        backgroundColor:'#fff',
        borderRadius:10,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal:responsiveWidth(5)
    },
    
    borderwhiteButton:{
        height:responsiveHeight(5),
        width:responsiveWidth(35),
        borderColor:'#fff',
        borderWidth:1,
        borderRadius:10,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal:responsiveWidth(5)
    }

})