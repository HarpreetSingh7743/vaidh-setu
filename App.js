import React, { Component } from 'react';
import { Root } from 'native-base';

import AuthAppNav from './Src/Routes/Auth/AuthAppNav';
import UserStackNav from './Src/Routes/Main/User/UserStackNav';
import VaidhMainTabNav from './Src/Routes/Main/Vaidh/VaidhMainTabNav';
import YogaTeacherMainTabNav from './Src/Routes/Main/YogaTeacher/YogaTeacherMainTabNav';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    // console.disableYellowBox=true
    return (
      <Root>
        <VaidhMainTabNav />
      </Root>
    );
  }
}

export default App;

